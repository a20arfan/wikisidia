from django.shortcuts import render, redirect
from django.db import connection
from collections import namedtuple
# from .forms import *

# Create your views here.
def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()] 

# read list batch pengiriman
def listBatchPengiriman(request):
    cursor = connection.cursor()
    role_admin_satgas = False
    role_supplier = False
    role_petugas_distribusi = False
    role_petugas_faskes = False

    try:
        if (request.session['role'] == 'admin_satgas'):
            role_admin_satgas = True
        if (request.session['role'] == 'supplier'):
            role_supplier = True
        if (request.session['role'] == 'petugas_distribusi'):
            role_petugas_distribusi = True
        if (request.session['role'] == 'petugas_faskes'):
            role_petugas_faskes = True
    except:
        return redirect('/')

    username = str(request.session['username'])
    cursor.execute("set search_path to wikisidia")
    cursor.execute("select DISTINCT ON(psdf.no_transaksi_sumber_daya) psdf.no_transaksi_sumber_daya, nama,coalesce(kode_faskes_nasional, '-') as kode_faskes_nasional,  total_berat, r.kode_status_permohonan as kode_sp,  coalesce(rsp.kode_status_batch_pengiriman, '-') as kode_status_batch_pengiriman from permohonan_sumber_daya_faskes psdf left join riwayat_status_permohonan r on no_transaksi_sumber_daya = r.nomor_permohonan left join transaksi_sumber_daya tsd on no_transaksi_sumber_daya = nomor left join petugas_faskes on username =username_petugas_faskes left join faskes on username_petugas = username left join pengguna p on psdf.username_petugas_faskes= p.username left join batch_pengiriman bp on psdf.no_transaksi_sumber_daya= bp.nomor_transaksi_sumber_daya left join riwayat_status_pengiriman rsp on rsp.kode_batch=bp.kode order by psdf.no_transaksi_sumber_daya asc, tsd.tanggal desc, rsp.tanggal desc")
    hasil = namedtuplefetchall(cursor)
    cursor.close()
    argument = {
        'table' : hasil,
        'role_admin_satgas' : role_admin_satgas,
        'role_supplier' : role_supplier,
        'role_petugas_distribusi' : role_petugas_distribusi,
        'role_petugas_faskes' : role_petugas_faskes,
    }
    return render(request, "listBatchPengiriman.html", argument)

    # input faskes
def getInputBatchPengiriman(request):
    input = request.POST
    nomor = input['nomor']
    return [nomor]

# create new batch_pengiriman
def createBatchPengiriman(request, no_transaksi_sumber_daya):
    message =""

    no_transaksi_sumber_daya= str(no_transaksi_sumber_daya)
    cursor = connection.cursor()
    role_admin_satgas = False
    role_supplier = False
    role_petugas_distribusi = False
    role_petugas_faskes = False

    try:
        if (request.session['role'] == 'admin_satgas'):
            role_admin_satgas = True
        if (request.session['role'] == 'supplier'):
            role_supplier = True
        if (request.session['role'] == 'petugas_distribusi'):
            role_petugas_distribusi = True
        if (request.session['role'] == 'petugas_faskes'):
            role_petugas_faskes = True
    except:
        return redirect('/')

    username = str(request.session['username'])
    cursor.execute("set search_path to wikisidia")
    cursor.execute("SELECT kode, k.nama as namaa,  username_petugas_distribusi, p.nama as petugas, id_lokasi_asal, id_lokasi_tujuan from batch_pengiriman join kendaraan k on k.nomor = no_kendaraan join pengguna p on p.username = username_petugas_distribusi where nomor_transaksi_sumber_daya = '"+no_transaksi_sumber_daya+"' ")
    hasil = namedtuplefetchall(cursor)


    cursor.execute("select * from pengguna where username='"+username+"'")
    pengguna = namedtuplefetchall(cursor)

    cursor.execute
    cursor.close()
   
    no_transaksi_sumber_daya= str(no_transaksi_sumber_daya)
    cursor = connection.cursor()
    cursor.execute("set search_path to wikisidia")
    cursor.execute("SELECT concat('BP', (LPAD( (cast((substring(kode,3,5)) As int)+1)::text, 3, '0' ))) as kode FROM BATCH_PENGIRIMAN ORDER BY kode desc limit 1")
    kode_view = namedtuplefetchall(cursor)
    cursor.execute("select k.nomor, k.nama from kendaraan k where k.nomor not in (SELECT no_kendaraan FROM (SELECT DISTINCT ON (bp.kode) bp.kode,  kode_status_batch_pengiriman, no_kendaraan, tanggal  FROM RIWAYAT_STATUS_PENGIRIMAN JOIN BATCH_PENGIRIMAN bp ON bp.kode=kode_batch order BY kode asc, tanggal desc) as foo WHERE kode_status_batch_pengiriman ='OTW' OR kode_status_batch_pengiriman ='PRO') ")
    nomor_kendaraan_view = namedtuplefetchall(cursor)
    cursor.execute("select p.nama, pd.username, no_sim from petugas_distribusi pd join pengguna p on pd.username=p.username where pd.username not in (SELECT username_petugas_distribusi FROM (SELECT DISTINCT ON (bp.kode) bp.kode,  kode_status_batch_pengiriman, no_kendaraan, tanggal, username_petugas_distribusi  FROM RIWAYAT_STATUS_PENGIRIMAN JOIN BATCH_PENGIRIMAN bp ON bp.kode=kode_batch order BY kode asc, tanggal desc) as foo WHERE kode_status_batch_pengiriman ='OTW' OR kode_status_batch_pengiriman ='PRO')")
    petugas_distribusi = namedtuplefetchall(cursor)
    cursor.execute("select concat(jalan_no, ', ', kelurahan, ', ', kecamatan, ', ', kabkot, ', ', provinsi) as id_lokasi_asal from lokasi order by id asc")
    id_lokasi_asal_view = namedtuplefetchall(cursor)
    cursor.execute("select concat(jalan_no, ', ', kelurahan, ', ', kecamatan, ', ', kabkot, ', ', provinsi) as id_lokasi_tujuan from lokasi order by id asc")
    id_lokasi_tujuan_view = namedtuplefetchall(cursor)
    cursor.execute("select  jenis_kendaraan, nama, nomor, berat_maksimum  from (SELECT DISTINCT ON (k.nomor) k.nomor , bp.kode, tanggal,  nama, jenis_kendaraan, berat_maksimum, coalesce(kode_status_batch_pengiriman, '-') as kode_status_batch_pengiriman  FROM RIWAYAT_STATUS_PENGIRIMAN FULL JOIN BATCH_PENGIRIMAN bp ON bp.kode=kode_batch FULL JOIN kendaraan k on k.nomor=no_kendaraan order BY k.nomor, tanggal desc) as foo WHERE  kode_status_batch_pengiriman <>'PRO' AND kode_status_batch_pengiriman <>'OTW' ")
    
    nama_kendaraan = namedtuplefetchall(cursor)

    
    args = {
        'no_transaksi_sumber_daya' : no_transaksi_sumber_daya,
        'table' : hasil,
        'role_admin_satgas' : role_admin_satgas,
        'role_supplier' : role_supplier,
        'role_petugas_distribusi' : role_petugas_distribusi,
        'role_petugas_faskes' : role_petugas_faskes,


        'no_transaksi_sumber_daya': no_transaksi_sumber_daya,
        'kode_batch': kode_view,
        'nomor_kendaraan' : nomor_kendaraan_view,
        'id_lokasi_asal' :id_lokasi_asal_view,
        'id_lokasi_tujuan' :id_lokasi_tujuan_view,

        'hasil_kendaraan' : nama_kendaraan,
        'hasil2' : pengguna,
        'hasil_petugas' : petugas_distribusi,

        'message' : message
    }
    return render(request, "createBatchPengiriman.html", args)

# memasukkan kendaraan
def kendaraanBatchPengiriman(request, no_transaksi_sumber_daya, no_kendaraan):
    no_transaksi_sumber_daya= str(no_transaksi_sumber_daya)
    no_kendaraan= str(no_kendaraan)
    message =""
    no_transaksi_sumber_daya= str(no_transaksi_sumber_daya)
    cursor = connection.cursor()
    role_admin_satgas = False
    role_supplier = False
    role_petugas_distribusi = False
    role_petugas_faskes = False
    try:
        if (request.session['role'] == 'admin_satgas'):
            role_admin_satgas = True
        if (request.session['role'] == 'supplier'):
            role_supplier = True
        if (request.session['role'] == 'petugas_distribusi'):
            role_petugas_distribusi = True
        if (request.session['role'] == 'petugas_faskes'):
            role_petugas_faskes = True
    except:
        return redirect('/')

    username = str(request.session['username'])
    cursor.execute("set search_path to wikisidia")
    cursor.execute("SELECT kode, k.nama as namaa,  username_petugas_distribusi, p.nama as petugas, id_lokasi_asal, id_lokasi_tujuan from batch_pengiriman join kendaraan k on k.nomor = no_kendaraan join pengguna p on p.username = username_petugas_distribusi where nomor_transaksi_sumber_daya = '"+no_transaksi_sumber_daya+"' ")
    hasil = namedtuplefetchall(cursor)
    # cursor.execute("SELECT kode, k.nama as namaa,  username_petugas_distribusi, p.nama, id_lokasi_asal, id_lokasi_tujuan from batch_pengiriman join kendaraan k on k.nomor = no_kendaraan join pengguna p on username = username_petugas_distribusi where nomor_transaksi_sumber_daya = '"+no_transaksi_sumber_daya+"' ")
    # hasil = namedtuplefetchall(cursor)

    cursor.execute("select * from pengguna where username='"+username+"'")
    pengguna = namedtuplefetchall(cursor)

    cursor.execute
    cursor.close()
   
    no_transaksi_sumber_daya= str(no_transaksi_sumber_daya)
    cursor = connection.cursor()
    cursor.execute("set search_path to wikisidia")
    cursor.execute("SELECT concat('BP', (LPAD( (cast((substring(kode,3,5)) As int)+1)::text, 3, '0' ))) as kode FROM BATCH_PENGIRIMAN ORDER BY kode desc limit 1")
    kode_view = namedtuplefetchall(cursor)
    cursor.execute("select k.nomor, k.nama from kendaraan k where k.nomor not in (SELECT no_kendaraan FROM (SELECT DISTINCT ON (bp.kode) bp.kode,  kode_status_batch_pengiriman, no_kendaraan, tanggal  FROM RIWAYAT_STATUS_PENGIRIMAN JOIN BATCH_PENGIRIMAN bp ON bp.kode=kode_batch order BY kode asc, tanggal desc) as foo WHERE kode_status_batch_pengiriman ='OTW' OR kode_status_batch_pengiriman ='PRO') ")
    nomor_kendaraan_view = namedtuplefetchall(cursor)
    cursor.execute("select p.nama, pd.username, no_sim from petugas_distribusi pd join pengguna p on pd.username=p.username where pd.username not in (SELECT username_petugas_distribusi FROM (SELECT DISTINCT ON (bp.kode) bp.kode,  kode_status_batch_pengiriman, no_kendaraan, tanggal, username_petugas_distribusi  FROM RIWAYAT_STATUS_PENGIRIMAN JOIN BATCH_PENGIRIMAN bp ON bp.kode=kode_batch order BY kode asc, tanggal desc) as foo WHERE kode_status_batch_pengiriman ='OTW' OR kode_status_batch_pengiriman ='PRO')")
    petugas_distribusi = namedtuplefetchall(cursor)
    cursor.execute("select concat(jalan_no, ', ', kelurahan, ', ', kecamatan, ', ', kabkot, ', ', provinsi) as id_lokasi_asal from lokasi order by id asc")
    id_lokasi_asal_view = namedtuplefetchall(cursor)
    cursor.execute("select concat(jalan_no, ', ', kelurahan, ', ', kecamatan, ', ', kabkot, ', ', provinsi) as id_lokasi_tujuan from lokasi order by id asc")
    id_lokasi_tujuan_view = namedtuplefetchall(cursor)
    cursor.execute("select  jenis_kendaraan, nama, nomor, berat_maksimum  from (SELECT DISTINCT ON (k.nomor) k.nomor , bp.kode, tanggal,  nama, jenis_kendaraan, berat_maksimum, coalesce(kode_status_batch_pengiriman, '-') as kode_status_batch_pengiriman  FROM RIWAYAT_STATUS_PENGIRIMAN FULL JOIN BATCH_PENGIRIMAN bp ON bp.kode=kode_batch FULL JOIN kendaraan k on k.nomor=no_kendaraan order BY k.nomor, tanggal desc) as foo WHERE  kode_status_batch_pengiriman <>'PRO' AND kode_status_batch_pengiriman <>'OTW' ")
    
    nama_kendaraan = namedtuplefetchall(cursor)
    
    

    args = {
        'no_transaksi_sumber_daya' : no_transaksi_sumber_daya,
        'table' : hasil,
        'role_admin_satgas' : role_admin_satgas,
        'role_supplier' : role_supplier,
        'role_petugas_distribusi' : role_petugas_distribusi,
        'role_petugas_faskes' : role_petugas_faskes,

        'no_transaksi_sumber_daya': no_transaksi_sumber_daya,
        'kode_batch': kode_view,
        'nomor_kendaraan' : nomor_kendaraan_view,
        'id_lokasi_asal' :id_lokasi_asal_view,
        'id_lokasi_tujuan' :id_lokasi_tujuan_view,

        'hasil_kendaraan' : nama_kendaraan,
        'hasil2' : pengguna,
        'hasil_petugas' : petugas_distribusi,

        'message' : message,
        'no_transaksi_sumber_daya': no_transaksi_sumber_daya,
        'no_kendaraan':no_kendaraan
    }
    return render(request, "createBatchPengiriman.html", args)





# memasukkan petugas
def petugasBatchPengiriman(request, no_transaksi_sumber_daya, no_kendaraan, p_distribusi):
    message =""
    no_transaksi_sumber_daya= str(no_transaksi_sumber_daya)
    no_kendaraan= str(no_kendaraan)
    p_distribusi= str(p_distribusi)
    
    no_transaksi_sumber_daya= str(no_transaksi_sumber_daya)
    cursor = connection.cursor()
    role_admin_satgas = False
    role_supplier = False
    role_petugas_distribusi = False
    role_petugas_faskes = False
    try:
        if (request.session['role'] == 'admin_satgas'):
            role_admin_satgas = True
        if (request.session['role'] == 'supplier'):
            role_supplier = True
        if (request.session['role'] == 'petugas_distribusi'):
            role_petugas_distribusi = True
        if (request.session['role'] == 'petugas_faskes'):
            role_petugas_faskes = True
    
    except:
        return redirect('/')

    username = str(request.session['username'])
    cursor.execute("set search_path to wikisidia")
    cursor.execute("SELECT kode, k.nama as namaa,  username_petugas_distribusi, p.nama as petugas, id_lokasi_asal, id_lokasi_tujuan from batch_pengiriman join kendaraan k on k.nomor = no_kendaraan join pengguna p on p.username = username_petugas_distribusi where nomor_transaksi_sumber_daya = '"+no_transaksi_sumber_daya+"' ")
    hasil = namedtuplefetchall(cursor)
    # cursor.execute("SELECT kode, k.nama as namaa,  username_petugas_distribusi, id_lokasi_asal, id_lokasi_tujuan from batch_pengiriman join kendaraan k on k.nomor = no_kendaraan where nomor_transaksi_sumber_daya = '"+no_transaksi_sumber_daya+"' ")
    # hasil = namedtuplefetchall(cursor)

    cursor.execute("select * from pengguna where username='"+username+"'")
    petugas_satgas_view = namedtuplefetchall(cursor)

    cursor.execute
    cursor.close()
   
    no_transaksi_sumber_daya= str(no_transaksi_sumber_daya)
    cursor = connection.cursor()
    cursor.execute("set search_path to wikisidia")
    cursor.execute("SELECT concat('BP', (LPAD( (cast((substring(kode,3,5)) As int)+1)::text, 3, '0' ))) as kode FROM BATCH_PENGIRIMAN ORDER BY kode desc limit 1")
    kode_view = namedtuplefetchall(cursor)
    cursor.execute("select k.nomor, k.nama from kendaraan k where k.nomor not in (SELECT no_kendaraan FROM (SELECT DISTINCT ON (bp.kode) bp.kode,  kode_status_batch_pengiriman, no_kendaraan, tanggal  FROM RIWAYAT_STATUS_PENGIRIMAN JOIN BATCH_PENGIRIMAN bp ON bp.kode=kode_batch order BY kode asc, tanggal desc) as foo WHERE kode_status_batch_pengiriman ='OTW' OR kode_status_batch_pengiriman ='PRO') ")
    nomor_kendaraan_view = namedtuplefetchall(cursor)
    cursor.execute("select p.nama, pd.username, no_sim from petugas_distribusi pd join pengguna p on pd.username=p.username where pd.username not in (SELECT username_petugas_distribusi FROM (SELECT DISTINCT ON (bp.kode) bp.kode,  kode_status_batch_pengiriman, no_kendaraan, tanggal, username_petugas_distribusi  FROM RIWAYAT_STATUS_PENGIRIMAN JOIN BATCH_PENGIRIMAN bp ON bp.kode=kode_batch order BY kode asc, tanggal desc) as foo WHERE kode_status_batch_pengiriman ='OTW' OR kode_status_batch_pengiriman ='PRO')")
    petugas_distribusi = namedtuplefetchall(cursor)
    cursor.execute("select concat(jalan_no, ', ', kelurahan, ', ', kecamatan, ', ', kabkot, ', ', provinsi) as id_lokasi_asal, l.id from lokasi l order by id asc")
    id_lokasi_asal_view = namedtuplefetchall(cursor)
    cursor.execute("select concat(jalan_no, ', ', kelurahan, ', ', kecamatan, ', ', kabkot, ', ', provinsi) as id_lokasi_tujuan, l.id from lokasi l order by id asc")
    id_lokasi_tujuan_view = namedtuplefetchall(cursor)
    cursor.execute("select  jenis_kendaraan, nama, nomor, berat_maksimum  from (SELECT DISTINCT ON (k.nomor) k.nomor , bp.kode, tanggal,  nama, jenis_kendaraan, berat_maksimum, coalesce(kode_status_batch_pengiriman, '-') as kode_status_batch_pengiriman  FROM RIWAYAT_STATUS_PENGIRIMAN FULL JOIN BATCH_PENGIRIMAN bp ON bp.kode=kode_batch FULL JOIN kendaraan k on k.nomor=no_kendaraan order BY k.nomor, tanggal desc) as foo WHERE  kode_status_batch_pengiriman <>'PRO' AND kode_status_batch_pengiriman <>'OTW' ")
    
    nama_kendaraan = namedtuplefetchall(cursor)
    
    if request.method =='POST':
        # kode_batch = request.POST['kode_batch']
        
        # petugas_satgas = request.POST.get('petugas_satgas', False)
        kode_batch = request.POST.get('kode_batch', False)
        petugas_satgas = request.POST['petugas_satgas']
        # p_distribusi = request['p_distribusi']
        # no_transaksi_sumber_daya = request.POST['no_transaksi_sumber_daya']
        # nomor_transaksi_sumber_daya = request.POST['nomor_transaksi_sumber_daya']
        id_lokasi_asal= request.POST['id_lokasi_asal']
        id_lokasi_tujuan = request.POST['id_lokasi_tujuan']
        # no_kendaraan = request.POST.get('no_kendaraan', False)
        
        # nomor_kendaraan = request.POST['nomor_kendaraan']
        try:
            cursor = connection.cursor()
            cursor.execute("set search_path to wikisidia")
            queryy = "insert into batch_pengiriman values ('"+kode_batch+"','"+petugas_satgas+"','"+p_distribusi+"','-','"+no_transaksi_sumber_daya+"','" +id_lokasi_asal+ "','" +id_lokasi_tujuan+ "','" + no_kendaraan +"')"
            cursor.execute(queryy)
            cursor.close()
            # alert(query)
            # message = query
            # return redirect('createBatchPengiriman', no_transaksi_sumber_daya.id)
        except:
            print('FF')
            # message = queryy
            message = "Batch pengiriman untuk permohonan sumber daya ini sudah cukup."
            pass

    args = {
        'table' : hasil,
        'role_admin_satgas' : role_admin_satgas,
        'role_supplier' : role_supplier,
        'role_petugas_distribusi' : role_petugas_distribusi,
        'role_petugas_faskes' : role_petugas_faskes,

        'kode_batch': kode_view,
        'nomor_kendaraan' : nomor_kendaraan_view,
        'id_lokasi_asal' :id_lokasi_asal_view,
        'id_lokasi_tujuan' :id_lokasi_tujuan_view,

        'hasil_kendaraan' : nama_kendaraan,
        'petugas_satgas' : petugas_satgas_view,
        'hasil_petugas' : petugas_distribusi,

        'message' : message,
        'no_transaksi_sumber_daya': no_transaksi_sumber_daya,
        'no_kendaraan':no_kendaraan,
        'p_distribusi' : p_distribusi
    }
    return render(request, "createBatchPengiriman.html", args)







# create new batch_pengiriman
def detailBatchPengiriman(request, no_transaksi_sumber_daya):
    message =""

    no_transaksi_sumber_daya= str(no_transaksi_sumber_daya)
    cursor = connection.cursor()
    role_admin_satgas = False
    role_supplier = False
    role_petugas_distribusi = False
    role_petugas_faskes = False

    try:
        if (request.session['role'] == 'admin_satgas'):
            role_admin_satgas = True
        if (request.session['role'] == 'supplier'):
            role_supplier = True
        if (request.session['role'] == 'petugas_distribusi'):
            role_petugas_distribusi = True
        if (request.session['role'] == 'petugas_faskes'):
            role_petugas_faskes = True
    except:
        return redirect('/')

    username = str(request.session['username'])
    cursor.execute("set search_path to wikisidia")
    cursor.execute("SELECT kode, k.nama as namaa,  username_petugas_distribusi, id_lokasi_asal, id_lokasi_tujuan from batch_pengiriman join kendaraan k on k.nomor = no_kendaraan where nomor_transaksi_sumber_daya = '"+no_transaksi_sumber_daya+"' ")
    hasil = namedtuplefetchall(cursor)


    cursor.execute("select * from pengguna where username='"+username+"'")
    pengguna = namedtuplefetchall(cursor)

    cursor.execute
    cursor.close()
   
    no_transaksi_sumber_daya= str(no_transaksi_sumber_daya)
    cursor = connection.cursor()
    cursor.execute("set search_path to wikisidia")
    cursor.execute("SELECT concat('BP', (LPAD( (cast((substring(kode,3,5)) As int)+1)::text, 3, '0' ))) as kode FROM BATCH_PENGIRIMAN ORDER BY kode desc limit 1")
    kode_view = namedtuplefetchall(cursor)
    cursor.execute("select k.nomor, k.nama from kendaraan k where k.nomor not in (SELECT no_kendaraan FROM (SELECT DISTINCT ON (bp.kode) bp.kode,  kode_status_batch_pengiriman, no_kendaraan, tanggal  FROM RIWAYAT_STATUS_PENGIRIMAN JOIN BATCH_PENGIRIMAN bp ON bp.kode=kode_batch order BY kode asc, tanggal desc) as foo WHERE kode_status_batch_pengiriman ='OTW' OR kode_status_batch_pengiriman ='PRO') ")
    nomor_kendaraan_view = namedtuplefetchall(cursor)
    cursor.execute("select p.nama, pd.username, no_sim from petugas_distribusi pd join pengguna p on pd.username=p.username where pd.username not in (SELECT username_petugas_distribusi FROM (SELECT DISTINCT ON (bp.kode) bp.kode,  kode_status_batch_pengiriman, no_kendaraan, tanggal, username_petugas_distribusi  FROM RIWAYAT_STATUS_PENGIRIMAN JOIN BATCH_PENGIRIMAN bp ON bp.kode=kode_batch order BY kode asc, tanggal desc) as foo WHERE kode_status_batch_pengiriman ='OTW' OR kode_status_batch_pengiriman ='PRO')")
    petugas_distribusi = namedtuplefetchall(cursor)
    cursor.execute("select concat(jalan_no, ', ', kelurahan, ', ', kecamatan, ', ', kabkot, ', ', provinsi) as id_lokasi_asal from lokasi order by id asc")
    id_lokasi_asal_view = namedtuplefetchall(cursor)
    cursor.execute("select concat(jalan_no, ', ', kelurahan, ', ', kecamatan, ', ', kabkot, ', ', provinsi) as id_lokasi_tujuan from lokasi order by id asc")
    id_lokasi_tujuan_view = namedtuplefetchall(cursor)
    cursor.execute("select  jenis_kendaraan, nama, nomor, berat_maksimum  from (SELECT DISTINCT ON (k.nomor) k.nomor , bp.kode, tanggal,  nama, jenis_kendaraan, berat_maksimum, coalesce(kode_status_batch_pengiriman, '-') as kode_status_batch_pengiriman  FROM RIWAYAT_STATUS_PENGIRIMAN FULL JOIN BATCH_PENGIRIMAN bp ON bp.kode=kode_batch FULL JOIN kendaraan k on k.nomor=no_kendaraan order BY k.nomor, tanggal desc) as foo WHERE  kode_status_batch_pengiriman <>'PRO' AND kode_status_batch_pengiriman <>'OTW' ")
    
    nama_kendaraan = namedtuplefetchall(cursor)

    
    args = {
        'no_transaksi_sumber_daya' : no_transaksi_sumber_daya,
        'table' : hasil,
        'role_admin_satgas' : role_admin_satgas,
        'role_supplier' : role_supplier,
        'role_petugas_distribusi' : role_petugas_distribusi,
        'role_petugas_faskes' : role_petugas_faskes,


        'no_transaksi_sumber_daya': no_transaksi_sumber_daya,
        'kode_batch': kode_view,
        'nomor_kendaraan' : nomor_kendaraan_view,
        'id_lokasi_asal' :id_lokasi_asal_view,
        'id_lokasi_tujuan' :id_lokasi_tujuan_view,

        'hasil_kendaraan' : nama_kendaraan,
        'hasil2' : pengguna,
        'hasil_petugas' : petugas_distribusi,

        'message' : message
    }
    return render(request, "detailBatchPengiriman.html", args)
