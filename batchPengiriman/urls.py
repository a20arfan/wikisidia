from django.contrib import admin
from django.urls import path
from .views import *

urlpatterns = [
    path('BatchPengiriman/', listBatchPengiriman, name="batchPengiriman"),
    path('createBatchPengiriman/<str:no_transaksi_sumber_daya>', createBatchPengiriman, name="createBatchPengiriman"),
    # path('post', views.postKendaraan),
    path('createBatchPengiriman/<str:no_transaksi_sumber_daya>/<str:no_kendaraan>', kendaraanBatchPengiriman, name="kendaraanBatchPengiriman"),
    path('createBatchPengiriman/<str:no_transaksi_sumber_daya>/<str:no_kendaraan>/<str:p_distribusi>', petugasBatchPengiriman, name="petugasBatchPengiriman"),
    path('detailBatchPengiriman/<str:no_transaksi_sumber_daya>', detailBatchPengiriman, name="detailBatchPengiriman"),
    
]