from django import forms

class createKendaraanForms(forms.Form):
    nomor = forms.CharField(label='Nomor Kendaraan', max_length=10, widget=forms.TextInput(attrs = {
        'class':'form-control',
        'type':'text',
        'required':True,
        'placeholder':'Masukkan kode Kendaraan',        
        'oninvalid' : "alert('Terdapat data yang belum diisi, silahkan lengkapi data terlebih dahulu! 🙏🏻😊🙏🏻')",
    }
    ))

    nama = forms.CharField(label='Nama', max_length=30, widget=forms.TextInput(attrs = {
        'class':'form-control',
        'type':'text',
        'required':True,
        'placeholder':'Masukkan nama Kendaraan',        
        'oninvalid' : "alert('Terdapat data yang belum diisi, silahkan lengkapi data terlebih dahulu! 🙏🏻😊🙏🏻')",
    }
    ))

    jenis_kendaraan = forms.CharField(label='Jenis Kendaraan', max_length=15, widget=forms.TextInput(attrs = {
        'class':'form-control',
        'type':'text',
        'required':True,
        'placeholder':'Masukkan jenis Kendaraan',        
        'oninvalid' : "alert('Terdapat data yang belum diisi, silahkan lengkapi data terlebih dahulu! 🙏🏻😊🙏🏻')",
    }
    ))

    berat_maksimum = forms.CharField(label='Berat Maksimum', widget=forms.NumberInput(attrs = {
        'class':'form-control',
        'type':'text',
        'required':True,
        'placeholder':'Masukkan berat maksimum (dalam kilogram)',        
        'oninvalid' : "alert('Terdapat data yang belum diisi, silahkan lengkapi data terlebih dahulu! 🙏🏻😊🙏🏻')",
    }
    ))
