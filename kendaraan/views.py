from django.shortcuts import render, redirect
from django.db import connection
from collections import namedtuple
from .forms import *

# Create your views here.
def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()] 

# read list kendaraan
def listKendaraan(request):
    cursor = connection.cursor()
    role_admin_satgas = False
    role_supplier = False
    role_petugas_distribusi = False
    role_petugas_faskes = False

    # cek_status = False

    try:
        if (request.session['role'] == 'admin_satgas'):
            role_admin_satgas = True
        if (request.session['role'] == 'supplier'):
            role_supplier = True
        if (request.session['role'] == 'petugas_distribusi'):
            role_petugas_distribusi = True
        if (request.session['role'] == 'petugas_faskes'):
            role_petugas_faskes = True
    except:
        return redirect('/')

    username = str(request.session['username'])
    cursor.execute("set search_path to wikisidia")
    cursor.execute("select row_number() over(order by nomor asc) as no, nomor, nama, jenis_kendaraan, berat_maksimum,  coalesce(kode_status_batch_pengiriman, '-') as kode_status_batch_pengiriman   from (SELECT DISTINCT ON (k.nomor) k.nomor , bp.kode, tanggal,  nama, jenis_kendaraan, berat_maksimum, kode_status_batch_pengiriman  FROM RIWAYAT_STATUS_PENGIRIMAN FULL JOIN BATCH_PENGIRIMAN bp ON bp.kode=kode_batch FULL JOIN kendaraan k on k.nomor=no_kendaraan order BY k.nomor, tanggal desc, bp.kode desc) as foo")
    hasil = namedtuplefetchall(cursor)

    

    cursor.execute

    cursor.close()
    argument = {
        'table' : hasil,
        'role_admin_satgas' : role_admin_satgas,
        'role_supplier' : role_supplier,
        'role_petugas_distribusi' : role_petugas_distribusi,
        'role_petugas_faskes' : role_petugas_faskes,
    }
    return render(request, "listKendaraan.html", argument)

# input kendaraan
def getInputKendaraan(request):
    input = request.POST
    nomor = input['nomor']
    return [nomor]

# delete kendaraan
def deleteKendaraan(request, nomor):
    nomor = str(nomor)
    cursor = connection.cursor()
    role_admin_satgas = False

    try:
        if (request.session['role'] == 'admin_satgas'):
            role_admin_satgas = True
    except:
        return redirect('/')

    cursor.execute("set search_path to wikisidia")
    cursor.execute("delete from kendaraan where nomor = '"+nomor+"'")
    cursor.execute("select row_number() over(order by nomor asc) as no, nomor, nama, jenis_kendaraan, berat_maksimum,  coalesce(kode_status_batch_pengiriman, '-') as kode_status_batch_pengiriman   from (SELECT DISTINCT ON (k.nomor) k.nomor , bp.kode, tanggal,  nama, jenis_kendaraan, berat_maksimum, kode_status_batch_pengiriman  FROM RIWAYAT_STATUS_PENGIRIMAN FULL JOIN BATCH_PENGIRIMAN bp ON bp.kode=kode_batch FULL JOIN kendaraan k on k.nomor=no_kendaraan order BY k.nomor, tanggal desc, bp.kode desc) as foo")    
    hasil = namedtuplefetchall(cursor)
    cursor.close()
    message = "Data berhasil dihapus"
    argument = {
        'table' : hasil,
        'role_admin_satgas' : role_admin_satgas,
        'message' : message
    }
    return render(request, "listKendaraan.html", argument)


# create new kendaraan
def createKendaraan(request):
    message =""
    cursor = connection.cursor()
    cursor.execute("set search_path to wikisidia")

    if request.method =='POST':
        input = getInputKendaraan(request)
        nomor = request.POST['nomor']
        nama = request.POST['nama']
        jenis_kendaraan = request.POST['jenis_kendaraan']
        berat_maksimum = request.POST['berat_maksimum']
        try :
            cursor = connection.cursor()
            cursor.execute("set search_path to wikisidia")
            cursor.execute("insert into kendaraan values ('"+input[0]+"','"+nama+"','"+jenis_kendaraan+"','"+berat_maksimum+"')")
            cursor.close()
            return redirect('/Kendaraan')
        except Exception as e:
            message = str(e)[80:]

    form = createKendaraanForms()
    args = {
        'form': form,
        'message' : message
    }
    return render(request, "createKendaraan.html", args)

# update kendaraan
def updateKendaraan(request, nomor_lama):
    message =""
    nomor_lama = str(nomor_lama)
    cursor = connection.cursor()
    cursor.execute("set search_path to wikisidia")
    

    if request.method =='POST':
        try:
            nomor = request.POST['nomor']
            nama = request.POST['nama']
            jenis_kendaraan = request.POST['jenis_kendaraan']
            berat_maksimum = request.POST['berat_maksimum']
            cursor = connection.cursor()
            cursor.execute("set search_path to wikisidia")
            query = "update kendaraan set nomor = '"+nomor+"', nama = '"+nama+"', jenis_kendaraan = '"+jenis_kendaraan+"', berat_maksimum ='"+berat_maksimum+"' where nomor = '" + nomor_lama + "' "
            cursor.execute(query)
            cursor.close()
            return redirect('/Kendaraan')
        except:
            print('FF')
            # message = query
            message = "Terdapat kesalahan data Kendaraan yang diupdate"
            pass

    form = createKendaraanForms()
    args = {
        'nomor_lama' : nomor_lama,
        'form': form,
        'message' : message
    }
    
    return render(request, "updateKendaraan.html", args)
