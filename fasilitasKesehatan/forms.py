from django import forms

class createFaskesForms(forms.Form):
    kode_faskes_nasional = forms.CharField(label='Kode Faskes', max_length=15, widget=forms.TextInput(attrs = {
        'class':'form-control',
        'type':'text',
        'required':True,
        'placeholder':'Masukkan kode Faskes',        
        'oninvalid' : "alert('Terdapat data yang belum diisi, silahkan lengkapi data terlebih dahulu! 🙏🏻😊🙏🏻')",
    }))