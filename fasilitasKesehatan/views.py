from django.shortcuts import render, redirect
from django.db import connection
from django.http import JsonResponse
from collections import namedtuple
from .forms import *

# Create your views here.

# general function
def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()] 

# read list fasilitas_kesehatan
def listFaskes(request):
    cursor = connection.cursor()
    role_admin_satgas = False
    role_supplier = False
    role_petugas_distribusi = False
    role_petugas_faskes = False

    try:
        if (request.session['role'] == 'admin_satgas'):
            role_admin_satgas = True
        if (request.session['role'] == 'supplier'):
            role_supplier = True
        if (request.session['role'] == 'petugas_distribusi'):
            role_petugas_distribusi = True
        if (request.session['role'] == 'petugas_faskes'):
            role_petugas_faskes = True
    except:
        return redirect('/')

    username = str(request.session['username'])
    cursor.execute("set search_path to wikisidia")
    cursor.execute("select kode_faskes_nasional, concat(jalan_no, ', ', kelurahan, ', ', kecamatan, ', ', kabkot, ', ', provinsi) as lokasi, nama, nama_tipe, kode_tipe_faskes from faskes join lokasi on id_lokasi = id join tipe_faskes on kode_tipe_faskes = kode join pengguna on username_petugas = username order by kode_faskes_nasional asc")
    hasil_list_faskes = namedtuplefetchall(cursor)
    cursor.close()
    argument = {
        'table' : hasil_list_faskes,
        'role_admin_satgas' : role_admin_satgas,
        'role_supplier' : role_supplier,
        'role_petugas_distribusi' : role_petugas_distribusi,
        'role_petugas_faskes' : role_petugas_faskes,
    }
    return render(request, "listFaskes.html", argument)

# input faskes
def getInputFaskes(request):
    input = request.POST
    kode_faskes_nasional = input['kode_faskes_nasional']
    return [kode_faskes_nasional]

# create new faskes
def createFaskes(request):
    message =""
    cursor = connection.cursor()
    cursor.execute("set search_path to wikisidia")
    cursor.execute("select id from lokasi order by id asc")
    id_lokasi_view = namedtuplefetchall(cursor)
    cursor.execute("select p.username, nama from pengguna p join petugas_faskes f on p.username = f.username where f.username not in (select username_petugas from faskes) order by username asc")
    petugas_faskes_view = namedtuplefetchall(cursor)
    cursor.execute("select kode from tipe_faskes order by cast(kode as char)")
    kode_tipe_faskes_view = namedtuplefetchall(cursor)
    cursor.execute("select * from lokasi order by id asc")
    lokasi_view = namedtuplefetchall(cursor)

    if request.method =='POST':
        input = getInputFaskes(request)
        id_lokasi = request.POST['id_lokasi']
        petugas_faskes = request.POST['petugas_faskes']
        kode_tipe_faskes = request.POST['kode_tipe_faskes']
        kode_faskes_nasional = request.POST['kode_faskes_nasional']
        try :
            cursor.execute("insert into faskes values ('"+input[0]+"','"+id_lokasi+"','"+petugas_faskes+"','"+kode_tipe_faskes+"')")
            cursor.close()
            return redirect('/Faskes')
        except Exception as e:
            message = str(e)[80:]

    form = createFaskesForms()
    args = {
        'id_lokasi': id_lokasi_view,
        'petugas_faskes': petugas_faskes_view,
        'kode_tipe_faskes': kode_tipe_faskes_view,
        'lokasi': lokasi_view,
        'form': form,
        'message' : message
    }
    return render(request, "createFaskes.html", args)

# delete faskes
def deleteFaskes(request, kode_faskes_nasional):
    kode_faskes_nasional = str(kode_faskes_nasional)
    cursor = connection.cursor()
    role_admin_satgas = False

    try:
        if (request.session['role'] == 'admin_satgas'):
            role_admin_satgas = True
    except:
        return redirect('/')

    cursor.execute("set search_path to wikisidia")
    cursor.execute("delete from faskes where kode_faskes_nasional = '"+kode_faskes_nasional+"'")
    cursor.execute("select kode_faskes_nasional, concat(jalan_no, ', ', kelurahan, ', ', kecamatan, ', ', kabkot, ', ', provinsi) as lokasi, nama, nama_tipe from faskes join lokasi on id_lokasi = id join tipe_faskes on kode_tipe_faskes = kode join pengguna on username_petugas = username order by kode_faskes_nasional asc")
    hasil = namedtuplefetchall(cursor)
    cursor.close()
    message = "Data berhasil dihapus"
    argument = {
        'table' : hasil,
        'role_admin_satgas' : role_admin_satgas,
        'message' : message
    }
    return render(request, "listFaskes.html", argument)

# update faskes
def updateFaskes(request, kode_faskes_nasional):
    message = ""
    kode_faskes_nasional = str(kode_faskes_nasional)
    cursor = connection.cursor()
    cursor.execute("set search_path to wikisidia")
    cursor.execute("select id from lokasi order by id asc")
    id_lokasi_view = namedtuplefetchall(cursor)
    cursor.execute("select p.username, nama from pengguna p join petugas_faskes f on p.username = f.username where f.username not in (select username_petugas from faskes) order by username asc")
    petugas_faskes_view = namedtuplefetchall(cursor)
    cursor.execute("select kode from tipe_faskes order by cast(kode as char)")
    kode_tipe_faskes_view = namedtuplefetchall(cursor)

    if request.method =='POST':
        try:
            input = request.POST
            cursor = connection.cursor()
            cursor.execute("set search_path to wikisidia")
            query = "update faskes"
            query += (" set kode_faskes_nasional = '" + kode_faskes_nasional + "' ")
            query += (", id_lokasi = '" + input['id_lokasi'] + "' ")
            query += (", username_petugas = '" + input['petugas_faskes'] + "' ")
            query += (", kode_tipe_faskes = '" + input['kode_tipe_faskes'] + "' ")
            query += ("where kode_faskes_nasional = '" + kode_faskes_nasional + "' ")
            cursor.execute(query)
            return redirect('/Faskes')
        except:
            message = "Terdapat kesalahan data Faskes yang diupdate"
            pass
    
    args = {
        'kode_faskes_nasional': kode_faskes_nasional,
        'id_lokasi': id_lokasi_view,
        'petugas_faskes': petugas_faskes_view,
        'kode_tipe_faskes': kode_tipe_faskes_view,
        'message' : message
    }
    
    return render(request, "updateFaskes.html", args)

# generate otomatis alamat dari id_lokasi
def getLokasi(request, id_lokasi):
    cursor = connection.cursor()
    cursor.execute("set search_path to wikisidia")
    cursor.execute("select concat(jalan_no, ', ', kelurahan, ', ', kecamatan, ', ', kabkot, ', ', provinsi) as lokasi from lokasi where id = '"+ id_lokasi +"'")
    hasil = namedtuplefetchall(cursor)
    cursor.close()
    argument = {
        'table' : hasil,
    }

    return JsonResponse(hasil, safe=False)

# generate otomatis nama tipe faskes
def getTipeFaskes(request, tipe):
    cursor = connection.cursor()
    cursor.execute("set search_path to wikisidia")
    cursor.execute("select nama_tipe from tipe_faskes where kode = '"+ tipe +"'")
    hasil = namedtuplefetchall(cursor)
    cursor.close()
    argument = {
        'table' : hasil,
    }

    return JsonResponse(hasil, safe=False)
