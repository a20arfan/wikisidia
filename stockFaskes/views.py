from django.shortcuts import render, redirect
from django.db import connection
from collections import namedtuple
from django.http import JsonResponse
from .forms import createStokFaskesForms

# Create your views here.
def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def list_stok_faskes(request):
    cursor = connection.cursor()
    role_petugas_faskes = False
    role_admin_satgas = False

    try:
        if (request.session['role'] == 'admin_satgas'):
            role_admin_satgas = True
        if (request.session['role'] == 'petugas_faskes'):
            role_petugas_faskes = True
    except:
        return redirect('/')
    
    username = str(request.session['username'])
    cursor.execute("set search_path to wikisidia")

    if(request.session['role'] == 'admin_satgas') :
        query = "select sf.kode_faskes, tf.nama_tipe, its.nama, sf.jumlah, sf.kode_item_sumber_daya"
        query += " from stok_faskes sf, faskes f, tipe_faskes tf, item_sumber_daya its"
        query += " where sf.kode_faskes=f.kode_faskes_nasional and f.kode_tipe_faskes=tf.kode and sf.kode_item_sumber_daya=its.kode"
        query += " order by sf.kode_faskes"
        cursor.execute(query)
    if(request.session['role'] == 'petugas_faskes') :
        query = "select sf.kode_faskes, tf.nama_tipe, its.nama, sf.jumlah, sf.kode_item_sumber_daya"
        query += " from stok_faskes sf, faskes f, tipe_faskes tf, item_sumber_daya its"
        query += " where sf.kode_faskes=f.kode_faskes_nasional and f.kode_tipe_faskes=tf.kode and sf.kode_item_sumber_daya=its.kode and f.username_petugas='" + username + "'"
        query += " order by sf.kode_faskes"
        cursor.execute(query)
    
    hasil = namedtuplefetchall(cursor)
    cursor.close()
    argument = {
        'table' : hasil,
        'role_admin_satgas' : role_admin_satgas,
        'role_petugas_faskes' : role_petugas_faskes,
    }
    return render(request, "listStokFaskes.html", argument)

def delete_stok_faskes(request, kode_faskes, kode_item_sumber_daya):
    kode_faskes = str(kode_faskes)
    kode_item_sumber_daya = str(kode_item_sumber_daya)
    cursor = connection.cursor()
    role_admin_satgas = False

    try:
        if (request.session['role'] == 'admin_satgas'):
            role_admin_satgas = True
    except:
        return redirect('/')
    
    username = str(request.session['username'])
    cursor.execute("set search_path to wikisidia")
    cursor.execute("delete from stok_faskes where kode_faskes='" + kode_faskes + "' and kode_item_sumber_daya='" + kode_item_sumber_daya + "'")
    query = "select sf.kode_faskes, tf.nama_tipe, its.nama, sf.jumlah, sf.kode_item_sumber_daya"
    query += " from stok_faskes sf, faskes f, tipe_faskes tf, item_sumber_daya its"
    query += " where sf.kode_faskes=f.kode_faskes_nasional and f.kode_tipe_faskes=tf.kode and sf.kode_item_sumber_daya=its.kode"
    query += " order by sf.kode_faskes"
    cursor.execute(query)

    hasil = namedtuplefetchall(cursor)
    cursor.close()
    message = "Data berhasil dihapus"
    argument = {
        'table' : hasil,
        'role_admin_satgas' : role_admin_satgas,
        'message': message,
    }
    return render(request, "listStokFaskes.html", argument)

def get_jumlah(request):
    input = request.POST
    jumlah = input['jumlah']
    return [jumlah]

def get_nama_faskes(request, kode_faskes):
    cursor = connection.cursor()
    cursor.execute("set search_path to wikisidia")
    cursor.execute("select nama_tipe from tipe_faskes where kode = '"+ kode_faskes +"'")
    hasil = namedtuplefetchall(cursor)
    cursor.close()
    argument = {
        'table' : hasil,
    }

    return JsonResponse(hasil, safe=False)

def get_nama_item(request, kode_item):
    cursor = connection.cursor()
    cursor.execute("set search_path to wikisidia")
    cursor.execute("select nama from item_sumber_daya where kode = '" + kode_item + "'")
    hasil = namedtuplefetchall(cursor)
    argument = {
        'table' : hasil,
    }

    return JsonResponse(hasil, safe=False)

def create_stok_faskes(request):
    message = ''
    cursor = connection.cursor()
    role_admin_satgas = False

    try:
        if (request.session['role'] == 'admin_satgas'):
            role_admin_satgas = True
    except:
        return redirect('/')
    
    cursor.execute("set search_path to wikisidia")
    cursor.execute("select kode_faskes_nasional from faskes order by kode_faskes_nasional")
    kode_faskes_view = namedtuplefetchall(cursor)
    cursor.execute("select kode from item_sumber_daya order by kode")
    kode_item_view = namedtuplefetchall(cursor)

    if request.method == 'POST':
        kode_faskes = request.POST['kode_faskes']
        kode_item = request.POST['kode_item']
        jumlah = get_jumlah(request)
        try :
            cursor.execute("insert into stok_faskes values ('"+kode_faskes+"','"+kode_item+"','"+jumlah[0]+"')")
            cursor.close()
            return redirect('/stokFaskes')
        except Exception as e:
            message = str(e)[80:]

    form = createStokFaskesForms()
    argument = {
        'kode_faskes': kode_faskes_view,
        'kode_item': kode_item_view,
        'form': form,
        'message' : message,
        'role_admin_satgas' : role_admin_satgas,
    }
    return render(request, "createStokFaskes.html", argument)

def update_stok_faskes(request, kode_faskes, kode_item_sumber_daya):
    message = ""
    kode_faskes = str(kode_faskes)
    kode_item_sumber_daya = str(kode_item_sumber_daya)
    cursor = connection.cursor()
    role_admin_satgas = False

    try:
        if (request.session['role'] == 'admin_satgas'):
            role_admin_satgas = True
    except:
        return redirect('/')
    
    cursor.execute("set search_path to wikisidia")
    query = "select distinct tf.nama_tipe from tipe_faskes tf, faskes f, stok_faskes sf"
    query += " where tf.kode=f.kode_tipe_faskes and f.kode_faskes_nasional=sf.kode_faskes and sf.kode_faskes='" + kode_faskes + "'"
    cursor.execute(query)
    tipe_faskes = namedtuplefetchall(cursor)[0].nama_tipe
    cursor.execute("select nama from item_sumber_daya where kode='" + kode_item_sumber_daya + "'")
    item = namedtuplefetchall(cursor)[0].nama

    if request.method == 'POST':
        jumlah = get_jumlah(request)
        try:
            query = "update stok_faskes set jumlah='" + jumlah[0] + "'"
            query += " where kode_faskes='" + kode_faskes + "' and kode_item_sumber_daya='" + kode_item_sumber_daya + "'"
            cursor.execute(query)
            cursor.close()
            return redirect('/stokFaskes')
        except:
            message = "Masukkan data yang valid"

    form = createStokFaskesForms()
    argument = {
        'tipe_faskes': tipe_faskes,
        'item': item,
        'role_admin_satgas' : role_admin_satgas,
        'kode_faskes': kode_faskes,
        'form': form,
        'kode_item_sumber_daya': kode_item_sumber_daya,
        'message': message,
    }
    return render(request, "updateStokFaskes.html", argument)