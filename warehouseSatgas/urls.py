from django.contrib import admin
from django.urls import path
from .views import *

urlpatterns = [
    path('WarehouseSatgas/', listWarehouseSatgas, name="listWarehouseSatgas"),
    path('WarehouseSatgas/CreateWarehouseSatgas/', createWarehouseSatgas, name="createWarehouseSatgas"),
    path('deleteWarehouseSatgas/<str:id_lokasi>', deleteWarehouseSatgas, name="deleteWarehouseSatgas"),
    path('getLokasiSatgas/<str:id_lokasi>', getLokasiSatgas, name="getLokasiSatgas"),
    path('getIdLokasi/<str:provinsi>', getIdLokasi, name="getIdLokasi"),
    path('updateWarehouseSatgas/<str:provinsi>/<str:id>', updateWarehouseSatgas, name="updateWarehouseSatgas")
]