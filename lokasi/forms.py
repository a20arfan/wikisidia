from django import forms

PROVINSI_CHOICE=[
    ('Aceh', 'Aceh'),
	('Sumatra Utara', 'Sumatra Utara'),
	('Sumatra Barat', 'Sumatra Barat'),
	('Riau', 'Riau'),
	('Kepulauan Riau', 'Kepulauan Riau'),
	('Jambi', 'Jambi'),
	('Bengkulu', 'Bengkulu'),
	('Sumatra Selatan', 'Sumatra Selatan'),
	('Kepulauan Bangka Belitung', 'Kepulauan Bangka Belitung'),
    ('Lampung', 'Lampung'),
    ('Banten', 'Banten'),
    ('Jawa Barat', 'Jawa Barat'),
    ('DKI Jakarta', 'DKI Jakarta'),
    ('Jawa Tengah', 'Jawa Tengah'),
    ('DIY Yogyakarta', 'DIY Yogyakarta'),
    ('Jawa Timur', 'Jawa Timur'),
    ('Bali', 'Bali'),
    ('Nusa Tenggara Barat', 'Nusa Tenggara Barat'),
    ('Nusa Tenggara Timur', 'Nusa Tenggara Timur'),
    ('Kalimantan Barat', 'Kalimantan Barat'),
    ('Kalimantan Selatan', 'Kalimantan Selatan'),
    ('Kalimantan Tengah', 'Kalimantan Tengah'),
    ('Kalimantan Timur', 'Kalimantan Timur'),
    ('Kalimantan Utara', 'Kalimantan Utara'),
    ('Gorontalo', 'Gorontalo'),
    ('Sulawesi Barat', 'Sulawesi Barat'),
    ('Sulawesi Selatan', 'Sulawesi Selatan'),
    ('Sulawesi Tengah', 'Sulawesi Tengah'),
    ('Sulawesi Tenggara', 'Sulawesi Tenggara'),
    ('Sulawesi Utara', 'Sulawesi Utara'),
    ('Maluku', 'Maluku'),
    ('Maluku Utara', 'Maluku Utara'),
    ('Papua Barat', 'Papua Barat'),
    ('Papua', 'Papua'),
]

class createLokasiForms(forms.Form):
    provinsi = forms.CharField(label='Provinsi', widget=forms.Select(choices=PROVINSI_CHOICE, attrs = {
            'class' : 'form-control',
            'type' : 'text',
            'required' : True,
            'oninvalid' : "alert('Terdapat data yang belum diisi, silahkan lengkapi data terlebih dahulu! 🙏🏻😊🙏🏻')",
    }))
    kabkot = forms.CharField(label='Kabupaten/Kota', max_length=20, widget = forms.TextInput(attrs = {
        'class' : 'form-control',
        'type' : 'text',
        'required' : True,
        'placeholder':'Masukkan nama kabupaten/kota',        
        'oninvalid' : "alert('Terdapat data yang belum diisi, silahkan lengkapi data terlebih dahulu! 🙏🏻😊🙏🏻')",
    }))
    kecamatan = forms.CharField(label='Kecamatan', max_length=20, widget = forms.TextInput(attrs = {
        'class' : 'form-control',
        'type' : 'text',
        'required' : True,
        'placeholder':'Masukkan nama kecamatan',        
        'oninvalid' : "alert('Terdapat data yang belum diisi, silahkan lengkapi data terlebih dahulu! 🙏🏻😊🙏🏻')",
    }))
    kelurahan = forms.CharField(label = 'Kelurahan', max_length=20, widget = forms.TextInput(attrs = {
        'class' : 'form-control',
        'type' : 'text',
        'required' : True,
        'placeholder':'Masukkan nama kelurahan',        
        'oninvalid' : "alert('Terdapat data yang belum diisi, silahkan lengkapi data terlebih dahulu! 🙏🏻😊🙏🏻')",
    }))
    jalan_no = forms.CharField(label = 'Jalan', max_length=20, widget = forms.TextInput(attrs = {
        'class' : 'form-control',
        'type' : 'text',
        'required' : True,
        'placeholder':'Masukkan nama dan nomor jalan',        
        'oninvalid' : "alert('Terdapat data yang belum diisi, silahkan lengkapi data terlebih dahulu! 🙏🏻😊🙏🏻')",
    }))