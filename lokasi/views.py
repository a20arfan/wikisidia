from django.shortcuts import render, redirect
from django.db import connection
from collections import namedtuple
from .forms import *

# Create your views here.
def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()] 

# read list batch pengiriman
def listLokasi(request):
    cursor = connection.cursor()
    role_admin_satgas = False
    role_supplier = False
    role_petugas_distribusi = False
    role_petugas_faskes = False

    try:
        if (request.session['role'] == 'admin_satgas'):
            role_admin_satgas = True
        if (request.session['role'] == 'supplier'):
            role_supplier = True
        if (request.session['role'] == 'petugas_distribusi'):
            role_petugas_distribusi = True
        if (request.session['role'] == 'petugas_faskes'):
            role_petugas_faskes = True
    except:
        return redirect('/')

    username = str(request.session['username'])
    cursor.execute("set search_path to wikisidia")
    cursor.execute("select id, provinsi, kabkot, kecamatan, kelurahan, jalan_no from lokasi order by id asc")
    hasil = namedtuplefetchall(cursor)
    cursor.close()
    argument = {
        'table' : hasil,
        'role_admin_satgas' : role_admin_satgas,
        'role_supplier' : role_supplier,
        'role_petugas_distribusi' : role_petugas_distribusi,
        'role_petugas_faskes' : role_petugas_faskes,
    }
    return render(request, "listLokasi.html", argument)

    # input faskes
def getInputLokasi(request):
    input = request.POST
    id = input['id']
    return [id]

# create new faskes
def createLokasi(request):
    message =""
    cursor = connection.cursor()
    cursor.execute("set search_path to wikisidia")
    cursor.execute("SELECT concat('Loc', (LPAD( (cast((substring(id,4,5)) As int)+1)::text, 2, '0' ))) as id_lokasi FROM LOKASI ORDER BY id desc limit 1")
    id_lokasi_view = namedtuplefetchall(cursor)
   

    if request.method =='POST':
        # input = getInputLokasi(request)
        id_lokasi = request.POST.get('id_lokasi', False)
        # id = request.POST['id']
        provinsi = request.POST['provinsi']
        kabkot= request.POST['kabkot']
        kecamatan = request.POST['kecamatan']
        kelurahan = request.POST['kelurahan']
        jalan_no = request.POST['jalan_no']
        try:
            cursor = connection.cursor()
            cursor.execute("set search_path to wikisidia")
            query = "insert into lokasi values ('"+id_lokasi+"','"+provinsi+"','"+kabkot+"','"+kecamatan+"','"+kelurahan+"','"+jalan_no+"')"
            cursor.execute(query)
            cursor.close()
            return redirect('/Lokasi')
        except Exception as e:
            # print('FF')
            # message = query
            # pass
            message = str(e)[80:]

    form = createLokasiForms()
    args = {
        'id_lokasi': id_lokasi_view,
        # 'provinsi': provinsi,
        # 'kabkot': kabkot,
        # 'kecamatan': kecamatan,
        # 'kelurahan': kelurahan,
        # 'jalan_no': jalan_no,
        'form': form,
        'message' : message
    }
    return render(request, "createLokasi.html", args)


def updateLokasi(request, id_lokasi):
    message =""
    id_lokasi = str(id_lokasi)
    cursor = connection.cursor()
    cursor.execute("set search_path to wikisidia")
    # cursor.execute("select id from lokasi order by id asc")
    # id_lokasi_view = namedtuplefetchall(cursor)

    if request.method =='POST':
        try:
            # id_lokasi = request.POST.get('id_lokasi', False)
            provinsi = request.POST['provinsi']
            kabkot= request.POST['kabkot']
            kecamatan = request.POST['kecamatan']
            kelurahan = request.POST['kelurahan']
            jalan_no = request.POST['jalan_no']
            cursor = connection.cursor()
            cursor.execute("set search_path to wikisidia")
            query = "update lokasi set provinsi = '"+provinsi+"', kabkot = '"+kabkot+"', kecamatan = '"+kecamatan+"', kelurahan ='"+kelurahan+"', jalan_no ='"+jalan_no+"' where id = '" + id_lokasi + "' "
            cursor.execute(query)
            cursor.close()
            return redirect('/Lokasi')
        except:
            print('FF')
            # message = query
            message = "Terdapat kesalahan data Kendaraan yang diupdate"
            pass

    form = createLokasiForms()
    args = {
        'id_lokasi' : id_lokasi,
        'form': form,
        'message' : message
    }
    
    return render(request, "updateLokasi.html", args)