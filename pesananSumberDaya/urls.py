from django.contrib import admin
from django.urls import path
from .views import detailPesananSD, listPesananSD, updatePesananSD, deletePesananSD, createPesananSD, rejectPesananSD, processPesananSD, selesaiMasalahPesananSD, selesaiPesananSD, riwayatPesananSD, deleteItemDiPesanan, get_item_from_supplier

urlpatterns = [
    path('pesanan/', listPesananSD, name="listPesananSD"),
    path('createPesanan/', createPesananSD, name="createPesananSD"),
    path('detailPesanan/TOR<str:nomor>/', detailPesananSD, name="detailPesananSD"),
    path('updatePesanan/TOR<str:nomor>/', updatePesananSD, name="updatePesananSD"),
    path('deleteItem/<str:nomor>/<str:no_urut>/', deleteItemDiPesanan, name="deleteItemDiPesanan"),
    path('deletePesanan/TOR<str:nomor>/', deletePesananSD, name="deletePesananSD"),
    path('riwayatPesanan/TOR<str:nomor>/', riwayatPesananSD, name="riwayatPesananSD"),
    path('rejectPesanan/TOR<str:nomor>/', rejectPesananSD, name="rejectPesananSD"),
    path('processPesanan/TOR<str:nomor>/', processPesananSD, name="processPesananSD"),
    path('selesaiMasalahPesanan/TOR<str:nomor>/', selesaiMasalahPesananSD, name="selesaiMasalahPesananSD"),
    path('selesaiPesanan/TOR<str:nomor>/', selesaiPesananSD, name="selesaiPesananSD"),
    path('getItem/<str:username_supplier>', get_item_from_supplier, name="get_item_from_supplier"),
]