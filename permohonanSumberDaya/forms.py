from django import forms
from django.db import connection
from collections import namedtuple

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def daftarItemPermohonanChoice():
    cursor = connection.cursor()
    cursor.execute("set search_path to wikisidia")
    cursor.execute("select max(no_transaksi_sumber_daya)+1 as no_transaksi_sumber_daya from permohonan_sumber_daya_faskes")
    data_no_transaksi_sumber_daya = namedtuplefetchall(cursor)
    cursor.execute("select nama from pengguna order by nama")
    data_username_petugas_faskes = namedtuplefetchall(cursor)
    cursor.execute("select max(no_urut)+1 as no_urut from daftar_item")
    no_urut_new = namedtuplefetchall(cursor)
    cursor.execute("select kode from item_sumber_daya order by kode")
    kode_item = namedtuplefetchall(cursor)

    data_no_transaksi_sumber_daya_choice = []
    data_username_petugas_faskes_choice = []
    no_urut_new_choice = []
    kode_item_choice = []

    idx_string = ""

    for a in data_no_transaksi_sumber_daya:
        idx_string = str(a.no_transaksi_sumber_daya)
        data_no_transaksi_sumber_daya_choice.append((idx_string, idx_string))
        
    for a in data_username_petugas_faskes:
        idx_string = str(a.nama)
        data_username_petugas_faskes_choice.append((idx_string, idx_string))

    for a in no_urut_new:
        idx_string = str(a.no_urut)
        no_urut_new_choice.append((idx_string, idx_string))

    for a in kode_item:
        idx_string = str(a.kode)
        kode_item_choice.append((idx_string, idx_string))

    argument = [
        data_no_transaksi_sumber_daya_choice,
        data_username_petugas_faskes_choice,
        no_urut_new_choice,
        kode_item_choice
    ]
    return argument

daftarItemPermohonan = daftarItemPermohonanChoice()

class updateJumlahItemPermohonanForms(forms.Form):
    no_transaksi_sumber_daya = forms.CharField(label = 'Nomor Transaksi', widget = forms.Select(attrs={'readonly':'readonly'}, choices=daftarItemPermohonan[0]), required = True)
    nama = forms.CharField(label = 'Petugas', widget = forms.Select(attrs={'readonly':'readonly'}, choices=daftarItemPermohonan[1]), required = True)
    no_urut = forms.CharField(label = 'No. Urut', widget=forms.Select(attrs={'readonly':'readonly'}, choices=daftarItemPermohonan[2]), required=True)
    kode = forms.CharField(label = 'Kode Item', widget=forms.Select(choices=daftarItemPermohonan[3]), required=True)
    jumlah_item = forms.IntegerField(label = 'Jumlah yang Dibeli', widget = forms.NumberInput(), initial = 10)

class createPermohonanSumberDayaForms(forms.Form):
    # no_transaksi_sumber_daya = forms.CharField(label = 'Nomor Transaksi', widget = forms.Select(attrs={'readonly':'readonly'}, choices=daftarItemPermohonan[0]), required = True)
    # # nama = forms.CharField(label = 'Petugas', widget = forms.Select(attrs={'readonly':'readonly'}, choices=daftarItemPermohonan[1]), required = True)
    # nama = forms.CharField(label = 'Petugas', required=True)
    # no_urut = forms.CharField(label = 'No. Urut', widget=forms.Select(attrs={'readonly':'readonly'}, choices=daftarItemPermohonan[2]), required=True)
    # # no_urut = forms.CharField(label = 'Nomor Urut', required=True)
    # kode = forms.CharField(label = 'Kode Item', widget=forms.Select(choices=daftarItemPermohonan[3]), required=True)
    jumlah_item = forms.IntegerField(label = 'Harga Satuan', widget = forms.NumberInput(attrs={
        'class':'form-control',
        'id':'harga_satuan',
    }), initial = 0, required=True)

    catatan = forms.CharField(label='Nama Item', max_length=100, widget=forms.TextInput(attrs = {
        'class':'form-control',
        'id':'nama_item',
        'type':'text'
    }))