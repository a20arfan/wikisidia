from django.shortcuts import render, reverse, redirect
from django.db import connection
from collections import namedtuple
from django.http import HttpResponseRedirect
from datetime import date
from .forms import *
from django.db import IntegrityError

# Create your views here.

# general function
def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()] 

# read list permohonan_sumber_daya_faskes
def listPermohonanSD(request):
    cursor = connection.cursor()
    role_admin_satgas = False
    role_supplier = False
    role_petugas_distribusi = False
    role_petugas_faskes = False

    try:
        if (request.session['role'] == 'admin_satgas'):
            role_admin_satgas = True
        if (request.session['role'] == 'supplier'):
            role_supplier = True
        if (request.session['role'] == 'petugas_distribusi'):
            role_petugas_distribusi = True
        if (request.session['role'] == 'petugas_faskes'):
            role_petugas_faskes = True
    except:
        return redirect('/')

    username = str(request.session['username'])
    cursor.execute("set search_path to wikisidia")
    if (request.session['role'] == 'admin_satgas'):
        query = "select no_transaksi_sumber_daya, coalesce(kode_faskes_nasional, '-') as kode_faskes_nasional,"
        query += " tsd.tanggal as tgl, total_berat, total_item, catatan, r.kode_status_permohonan as kode_sp"
        query += " from permohonan_sumber_daya_faskes"
        query += " left join riwayat_status_permohonan r on no_transaksi_sumber_daya = r.nomor_permohonan"
        query += " left join transaksi_sumber_daya tsd on no_transaksi_sumber_daya = nomor"
        query += " left join petugas_faskes on username = username_petugas_faskes"
        query += " left join faskes on username_petugas = username"
        query += " order by no_transaksi_sumber_daya"
        cursor.execute(query)
    if (request.session['role'] == 'petugas_faskes'):
        query = "select psdf.no_transaksi_sumber_daya, coalesce(kode_faskes_nasional, '-') as kode_faskes_nasional,"
        query += " tsd.tanggal as tgl, total_berat, total_item, catatan, r.kode_status_permohonan as kode_sp,"
        query += " ti.nama as nama, di.no_urut, its.kode as kode"
        query += " from permohonan_sumber_daya_faskes psdf"
        query += " left join riwayat_status_permohonan r on psdf.no_transaksi_sumber_daya = r.nomor_permohonan"
        query += " left join transaksi_sumber_daya tsd on psdf.no_transaksi_sumber_daya = tsd.nomor"
        query += " left join petugas_faskes on username = username_petugas_faskes"
        query += " left join faskes on username_petugas = username"
        query += " left join daftar_item di on di.no_transaksi_sumber_daya = nomor"
        query += " left join item_sumber_daya its on its.kode = di.kode_item_sumber_daya"
        query += " left join tipe_item ti on ti.kode = its.kode_tipe_item"
        query += " where username_petugas_faskes = '"+username+"'"
        query += " order by no_transaksi_sumber_daya"
        cursor.execute(query)
    hasil = namedtuplefetchall(cursor)
    # cursor.execute("select di.no_urut, ti.nama as nama, di.jumlah_item as jumlah_item, psdf.no_transaksi_sumber_daya as no_transaksi_sumber_daya from daftar_item di left join item_sumber_daya on kode = di.kode_item_sumber_daya left join tipe_item ti on ti.kode = kode_tipe_item left join permohonan_sumber_daya_faskes psdf on psdf.no_transaksi_sumber_daya = di.no_transaksi_sumber_daya left join riwayat_status_permohonan on nomor_permohonan = psdf.no_transaksi_sumber_daya")
    # hasil1 = namedtuplefetchall(cursor)
    cursor.close()
    argument = {
        'table' : hasil,
        # 'table1' : hasil1,
        'role_admin_satgas' : role_admin_satgas,
        'role_supplier' : role_supplier,
        'role_petugas_distribusi' : role_petugas_distribusi,
        'role_petugas_faskes' : role_petugas_faskes,
    }
    return render(request, "listPermohonanSD.html", argument)

# detail
def detailPermohonan(request, no_transaksi_sumber_daya, kode_sp):
    no_transaksi_sumber_daya = str(no_transaksi_sumber_daya)
    kode_sp = str(kode_sp)
    cursor = connection.cursor()
    role_petugas_faskes = False
    role_admin_satgas = False

    try:
        if (request.session['role'] == 'petugas_faskes'):
            role_petugas_faskes = True
        if (request.session['role'] == 'admin_satgas'):
            role_admin_satgas = True
    except:
        return redirect('/')

    username = str(request.session['username'])
    cursor.execute("set search_path to wikisidia")
    cursor.execute("select distinct no_transaksi_sumber_daya, username_petugas_faskes, catatan, tanggal from permohonan_sumber_daya_faskes, riwayat_status_permohonan where nomor_permohonan = no_transaksi_sumber_daya and no_transaksi_sumber_daya = '"+ no_transaksi_sumber_daya +"' and kode_status_permohonan = '"+ kode_sp +"'")
    hasil = namedtuplefetchall(cursor)
    cursor.execute("select di.no_urut as no_urut, ti.nama as nama, di.jumlah_item as jumlah_item, psdf.no_transaksi_sumber_daya as no_transaksi_sumber_daya from daftar_item di left join item_sumber_daya on kode = di.kode_item_sumber_daya left join tipe_item ti on ti.kode =kode_tipe_item left join permohonan_sumber_daya_faskes psdf on psdf.no_transaksi_sumber_daya = di.no_transaksi_sumber_daya left join riwayat_status_permohonan on nomor_permohonan = di.no_transaksi_sumber_daya where di.no_transaksi_sumber_daya = '"+ no_transaksi_sumber_daya +"' and kode_status_permohonan = '"+ kode_sp +"'")
    hasil1 = namedtuplefetchall(cursor)
    cursor.close()
    argument = {
        'table' : hasil,
        'table1' : hasil1,
        'role_admin_satgas' : role_admin_satgas,
        'role_petugas_faskes' : role_petugas_faskes,
    }
    return render(request, 'detailPermohonan.html', argument)

# delete permohonan
def deletePermohonanSD(request, no_transaksi_sumber_daya):
    no_transaksi_sumber_daya = str(no_transaksi_sumber_daya)
    cursor = connection.cursor()
    role_petugas_faskes = False

    try:
        if (request.session['role'] == 'petugas_faskes'):
            role_petugas_faskes = True
    except:
        return redirect('/')

    username = str(request.session['username'])
    cursor.execute("set search_path to wikisidia")
    cursor.execute("delete from permohonan_sumber_daya_faskes using riwayat_status_permohonan where no_transaksi_sumber_daya = '"+ no_transaksi_sumber_daya +"' and kode_status_permohonan='REQ' and no_transaksi_sumber_daya = nomor_permohonan;")
    cursor.execute("delete from riwayat_status_permohonan using permohonan_sumber_daya_faskes where no_transaksi_sumber_daya = nomor_permohonan and nomor_permohonan = '"+ no_transaksi_sumber_daya +"' AND kode_status_permohonan='REQ'")
    cursor.execute("delete from transaksi_sumber_daya using permohonan_sumber_daya_faskes, riwayat_status_permohonan where no_transaksi_sumber_daya = nomor and no_transaksi_sumber_daya = nomor_permohonan and nomor = '"+ no_transaksi_sumber_daya +"' AND kode_status_permohonan='REQ'")

    query = "select psdf.no_transaksi_sumber_daya, coalesce(kode_faskes_nasional, '-') as kode_faskes_nasional,"
    query += " tsd.tanggal as tgl, total_berat, total_item, catatan, r.kode_status_permohonan as kode_sp,"
    query += " ti.nama as nama, di.no_urut, its.kode as kode"
    query += " from permohonan_sumber_daya_faskes psdf"
    query += " left join riwayat_status_permohonan r on psdf.no_transaksi_sumber_daya = r.nomor_permohonan"
    query += " left join transaksi_sumber_daya tsd on psdf.no_transaksi_sumber_daya = tsd.nomor"
    query += " left join petugas_faskes on username = username_petugas_faskes"
    query += " left join faskes on username_petugas = username"
    query += " left join daftar_item di on di.no_transaksi_sumber_daya = nomor"
    query += " left join item_sumber_daya its on its.kode = di.kode_item_sumber_daya"
    query += " left join tipe_item ti on ti.kode = its.kode_tipe_item"
    query += " where username_petugas_faskes = '"+username+"'"
    query += " order by no_transaksi_sumber_daya"
    cursor.execute(query)
    hasil = namedtuplefetchall(cursor)
    cursor.close()
    message = "Data berhasil dihapus"
    argument = {
        'table' : hasil,
        'role_petugas_faskes' : role_petugas_faskes,
        'message' : message
    }
    return render(request, "listPermohonanSD.html", argument)

# update Permohonan
def updatePermohonan(request, no_transaksi_sumber_daya, nama, no_urut, kode):
    message = ""
    # no_transaksi_sumber_daya = str(no_transaksi_sumber_daya)
    # kode_sp = str(kode_sp)
    today = date.today().strftime('%Y-%m-%d')

    if request.method =='POST':
        try:
            input = request.POST
            cursor = connection.cursor()
            cursor.execute("set search_path to wikisidia")
            query = "update daftar_item"
            query += (" set no_urut = '" + no_urut + "' ")
            query += (", jumlah_item = '" + input['jumlah_item'] + "' ")
            query += (", kode_item_sumber_daya = '" + input['kode'] + "' ")
            query += (" where no_transaksi_sumber_daya in (select psdf.no_transaksi_sumber_daya")
            query += (" from permohonan_sumber_daya_faskes psdf, riwayat_status_permohonan")
            query += (" where psdf.no_transaksi_sumber_daya = '" + no_transaksi_sumber_daya + "' and kode_status_permohonan = 'REQ') ")
            cursor.execute(query)
            query1 = "update transaksi_sumber_daya"
            query1 += (" set tanggal = '"+ input[today] +"' ")
            query1 += (" where nomor in (select psdf.no_transaksi_sumber_daya from permohonan_sumber_daya_faskes psdf, riwayat_status_permohonan ")
            query1 += (" where psdf.no_transaksi_sumber_daya = ' "+ no_transaksi_sumber_daya +" ' and kode_status_permohonan = 'REQ') ")
            cursor.execute(query1)
            return redirect('/PermohonanSumberDaya')
        except:
            print('FF')
            message = "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"
            pass
    
    cursor = connection.cursor()
    cursor.execute("set search_path to wikisidia")
    query = "select di.jumlah_item as jumlah_item from daftar_item di"
    query += " left join item_sumber_daya on kode = di.kode_item_sumber_daya"
    query += " left join tipe_item ti on ti.kode =kode_tipe_item"
    query += " left join permohonan_sumber_daya_faskes psdf on psdf.no_transaksi_sumber_daya = di.no_transaksi_sumber_daya"
    query += " left join riwayat_status_permohonan on nomor_permohonan = di.no_transaksi_sumber_daya"
    query += (" where psdf.no_transaksi_sumber_daya = '" + no_transaksi_sumber_daya + "' ")
    query += (" and no_urut = '" + no_urut + "' ")
    query += (" and kode_status_permohonan = 'REQ'")
    cursor.execute(query)
    hasil = namedtuplefetchall(cursor)

    form = updateJumlahItemPermohonanForms(initial={
        'no_transaksi_sumber_daya' : no_transaksi_sumber_daya,
        'nama' : nama,
        'no_urut' : no_urut,
        'kode' : kode,
        'jumlah_item' : hasil[0].jumlah_item,
    }) 

    cursor = connection.cursor()
    cursor.execute("set search_path to wikisidia")
    queryyy = "select no_urut, ti.nama, jumlah_item from daftar_item"
    queryyy += " join item_sumber_daya isd on isd.kode = kode_item_sumber_daya"
    queryyy += " join tipe_item ti on isd.kode_tipe_item = ti.kode"
    cursor.execute(queryyy)
    daftar_item_new = namedtuplefetchall(cursor)

    form.fields['no_transaksi_sumber_daya'].widget.attrs['disabled'] = 'disabled'
    form.fields['nama'].widget.attrs['disabled'] = 'disabled'
    form.fields['no_urut'].widget.attrs['disabled'] = 'disabled'

    argument = {
        'form' : form,
        'no_transaksi_sumber_daya': no_transaksi_sumber_daya,
        'nama' : nama,
        'no_urut' : no_urut,
        'kode' : kode,
        'message' : message,
        'daftar_item_new' : daftar_item_new
    }
    
    return render(request, "updatePermohonan.html", argument)


# riwayat Status
def riwayatStatus(request, no_transaksi_sumber_daya):
    no_transaksi_sumber_daya = str(no_transaksi_sumber_daya)
    cursor = connection.cursor()
    role_petugas_faskes = False
    role_admin_satgas = False

    try:
        if (request.session['role'] == 'petugas_faskes'):
            role_petugas_faskes = True
        if (request.session['role'] == 'admin_satgas'):
            role_admin_satgas = True
    except:
        return redirect('/')

    username = str(request.session['username'])
    cursor.execute("set search_path to wikisidia")
    query = "select row_number() over(order by kode_status_permohonan asc) as no,"
    query += " kode_status_permohonan, nama, username_admin, tanggal"
    query += " from status_permohonan, riwayat_status_permohonan"
    query += (" where nomor_permohonan = '"+ no_transaksi_sumber_daya +"' ")
    query += (" and kode_status_permohonan = kode")
    cursor.execute(query)
    hasil = namedtuplefetchall(cursor)
    cursor.close()
    argument = {
        'table' : hasil,
        'no_transaksi_sumber_daya' : no_transaksi_sumber_daya,
        'role_admin_satgas' : role_admin_satgas,
        'role_petugas_faskes' : role_petugas_faskes,
    }
    return render(request, 'riwayatStatus.html', argument)

# create riwayat Status
def buatRiwayatStatus(request, no_transaksi_sumber_daya, kode_sp, command):
    no_transaksi_sumber_daya = str(no_transaksi_sumber_daya)
    kode_sp = str(kode_sp)
    command = str(command)
    cursor = connection.cursor()
    role_admin_satgas = False
    today = date.today().strftime('%Y-%m-%d')

    try:
        if (request.session['role'] == 'admin_satgas'):
            role_admin_satgas = True
    except:
        return redirect('/')
    
    username = str(request.session['username'])
    cursor.execute("set search_path to wikisidia")
    
    if (kode_sp == 'REQ'):
        if (command == 'process'):
            query = "update riwayat_status_permohonan"
            query += (" set kode_status_permohonan = 'PRO' ")
            query += (", username_admin = '"+ username +"' ")
            query += (", tanggal = '"+ today +"' ")
            query += (" where nomor_permohonan = '"+ no_transaksi_sumber_daya +"' ")
        if (command == 'reject'):
            query = "update riwayat_status_permohonan"
            query += (" set kode_status_permohonan = 'REJ' ")
            query += (", username_admin = '"+ username +"' ")
            query += (", tanggal = '"+ today +"' ")
            query += (" where nomor_permohonan = '"+ no_transaksi_sumber_daya +"' ")
    if (kode_sp == 'PRO'):
        if (command == 'selesaidenganmasalah'):
            query = "update riwayat_status_permohonan"
            query += (" set kode_status_permohonan = 'MAS' ")
            query += (", username_admin = '"+ username +"' ")
            query += (", tanggal = '"+ today +"' ")
            query += (" where nomor_permohonan = '"+ no_transaksi_sumber_daya +"' ")
        if (command == 'selesai'):
            query = "update riwayat_status_permohonan"
            query += (" set kode_status_permohonan = 'FIN' ")
            query += (", username_admin = '"+ username +"' ")
            query += (", tanggal = '"+ today +"' ")
            query += (" where nomor_permohonan = '"+ no_transaksi_sumber_daya +"' ")

    cursor.execute(query)

    query = "select no_transaksi_sumber_daya, coalesce(kode_faskes_nasional, '-') as kode_faskes_nasional,"
    query += " tsd.tanggal as tgl, total_berat, total_item, catatan, r.kode_status_permohonan as kode_sp"
    query += " from permohonan_sumber_daya_faskes"
    query += " left join riwayat_status_permohonan r on no_transaksi_sumber_daya = r.nomor_permohonan"
    query += " left join transaksi_sumber_daya tsd on no_transaksi_sumber_daya = nomor"
    query += " left join petugas_faskes on username = username_petugas_faskes"
    query += " left join faskes on username_petugas = username"
    query += " order by no_transaksi_sumber_daya"
    cursor.execute(query)

    hasil = namedtuplefetchall(cursor)
    cursor.close()
    message = "Riwayat status baru untuk transaksi '"+ no_transaksi_sumber_daya +"' berhasil ditambahkan."
    argument = {
        'table' : hasil,
        'role_admin_satgas' : role_admin_satgas,
        'message' : message
    }
    return render(request, "listPermohonanSD.html", argument)

# input no transaksi
def getInputNomorTransaksi(request):
    input = request.POST
    kode_faskes_nasional = input['no_transaksi_sumber_daya']
    return [kode_faskes_nasional]

# update Permohonan
def buatPermohonan(request):
    message = ""
    # no_transaksi_sumber_daya = str(no_transaksi_sumber_daya)
    # kode_sp = str(kode_sp)
    # today = date.today().strftime('%Y-%m-%d')
    cursor = connection.cursor()
    username = str(request.session['username'])
    # no_transaksi_sumber_daya = '1'
    nama = username
    # no_urut = '1'
    kode = '1'

    cursor.execute("set search_path to wikisidia")
    queryy = "select max(no_transaksi_sumber_daya)+1 as no_transaksi_sumber_daya from permohonan_sumber_daya_faskes"
    cursor.execute(queryy)
    no_transaksi = namedtuplefetchall(cursor)

    if request.method =='POST':
        # try:
            input = request.POST
            cursor = connection.cursor()
            cursor.execute("set search_path to wikisidia")
            query = "insert into permohonan_sumber_daya_faskes values("
            query += ("'"+ no_transaksi[0] +"', '"+ username +"', '"+ input['catatan'] +"' ")
            cursor.execute(query)

            query1 = "insert into daftar_item values("
            query1 += ("'"+ no_transaksi[0] +"', DEFAULT, '"+ input['jumlah_item'] +"', '"+ input['kode'] +"' ")
            cursor.execute(query1)

            return redirect('/PermohonanSumberDaya')
        # except:
        #     print('FF')
        #     message = "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"
        #     no_transaksi_sumber_daya = no_transaksi[0]
        #     nama = username
        #     # no_urut = input['no_urut']
        #     kode = input['kode']
        #     pass
    
    cursor.execute("set search_path to wikisidia")
    query4 = "select no_urut, nama, jumlah_item from daftar_item, item_sumber_daya where kode = kode_item_sumber_daya order by no_urut"
    cursor.execute(query4)
    daftar_item = namedtuplefetchall(cursor)

    form = createPermohonanSumberDayaForms(initial={
        # 'no_transaksi_sumber_daya' : no_transaksi_sumber_daya,
        'nama' : nama,
        # 'no_urut' : no_urut,
        'kode' : kode,
        # 'jumlah_item' : hasil[0].jumlah_item,
        # 'catatan' : hasil1[0].catatan
    }) 

    form.fields['no_transaksi_sumber_daya'].widget.attrs['disabled'] = 'disabled'
    form.fields['nama'].widget.attrs['disabled'] = 'disabled'
    form.fields['no_urut'].widget.attrs['disabled'] = 'disabled'

    argument = {
        'form' : form,
        # 'no_transaksi_sumber_daya': no_transaksi_sumber_daya,
        'nama' : nama,
        # 'no_urut' : no_urut,
        'kode' : kode,
        'message' : message,
        'daftar_item': daftar_item
    }
    
    return render(request, "buatPermohonan.html", argument)

# get input jumlah item sumber daya
def getInputJumlahItem(request):
    input = request.POST
    jumlah_item = input['jumlah_item']
    return [jumlah_item]

# get input catatan
def getInputCatatan(request):
    input = request.POST
    catatan = input['catatan']
    return [catatan]

# create new item sumber daya
def createPermohonanSumberDaya(request):
    message =""
    cursor = connection.cursor()
    username = str(request.session['username'])
    nama_supplier = username
    cursor.execute("set search_path to wikisidia")
    cursor.execute("select kode from tipe_item")
    kode_tipe_item_view = namedtuplefetchall(cursor)

    cursor.execute("select no_transaksi_sumber_daya from permohonan_sumber_daya_faskes order by no_transaksi_sumber_daya desc")
    nomor_transaksi = namedtuplefetchall(cursor)[0]
    if (len(nomor_transaksi) == 0):
        no_transaksi = 1
    else:
        no_transaksi = nomor_transaksi.no_transaksi_sumber_daya+1
    no_transaksi_sumber_daya = str(no_transaksi)

    query = "select nomor from transaksi_sumber_daya, permohonan_sumber_daya_faskes where nomor = no_transaksi_sumber_daya order by nomor desc"
    cursor.execute(query)
    no_urut = namedtuplefetchall(cursor)
    if (len(no_urut) == 0):
        no_urut = 1
    else :
        no_urut = no_urut[0].nomor + 1
    # no_urut = str(no_urut)

    if request.method =='POST':
        inputjumlah = getInputJumlahItem(request)
        inputCatatan = getInputCatatan(request)
        kode_tipe_item = request.POST['kode_tipe_item']
        try :
            cursor.execute("set search_path to wikisidia")
            query = "insert into permohonan_sumber_daya_faskes values("
            query += ("'"+ no_transaksi_sumber_daya +"', '"+ username +"', '"+ inputCatatan[0] +"' )")
            cursor.execute(query)
            queryy = "select kode from item_sumber_daya, daftar_item where kode = kode_item_sumber_daya and no_transaksi_sumber_daya = '"+ no_transaksi_sumber_daya +"' and kode_tipe_item = '"+ kode_tipe_item +"'"
            cursor.execute(queryy)
            kode_item_sumber_daya = namedtuplefetchall(cursor)
            query1 = "insert into daftar_item values ('"+ no_transaksi_sumber_daya +"',"
            query1 += (" '"+ no_urut +"','"+ inputjumlah[0] +"', '"+ kode_item_sumber_daya +"')")
            cursor.execute(query1)
            cursor.close()
            # message = "Item '"+ inputNama[0] +"' berhasil tersimpan."
            return redirect('/PermohonanSumberDaya')
        except IntegrityError:
            message = "Item dengan kode '"+ kode_tipe_item +"' sudah tersedia."
        except :
            message = "Terdapat data yang belum diisi, silahkan lengkapi data terlebih dahulu."

    form = createPermohonanSumberDayaForms()
    # form.fields['nama_supplier'].widget.attrs['disabled'] = 'disabled'
    # form.fields['no_transaksi_sumber_daya'].widget.attrs['disabled'] = 'disabled'

    args = {
        'kode_tipe_item' : kode_tipe_item_view,
        # 'supplier' : supplier_view,
        'no_transaksi_sumber_daya' : no_transaksi_sumber_daya,
        'no_urut' : no_urut,
        'nama_supplier' : nama_supplier,
        'form': form,
        'message' : message
    }
    return render(request, "buatPermohonan.html", args)