from django import forms
from django.db import connection

class createItemSumberDayaForms(forms.Form):
    kode_item = forms.CharField(label = 'Kode Item', required=True)
    nama_supplier = forms.CharField(label = 'Supplier', required=True)
    nama_item = forms.CharField(label='Nama Item', max_length=30, widget=forms.TextInput(attrs = {
        'class':'form-control',
        'type':'text',
        'required':True,
        'placeholder':'Masukkan Nama Item',
    }))
    harga_satuan = forms.IntegerField(label = 'Harga Satuan', widget = forms.NumberInput(), initial = 0, required=True)
    berat_satuan = forms.IntegerField(label = 'Berat Satuan', widget = forms.NumberInput(), initial = 0, required=True)

class updateItemSumberDayaForms(forms.Form):
    kode_item = forms.CharField(label = 'Kode Item', max_length=15, widget= forms.TextInput (attrs={
        'class':'form-control',
        'id':'kode_item',
        'required':True
    }))

    nama_supplier = forms.CharField(label = 'Supplier', max_length=15, widget= forms.TextInput (attrs={
        'class':'form-control',
        'id':'supplier',
        'required':True
    }))

    nama_item = forms.CharField(label='Nama Item', max_length=100, widget=forms.TextInput(attrs = {
        'class':'form-control',
        'id':'nama_item',
        'type':'text',
        'required':True,
        'placeholder':'Masukkan Nama Item',
    }))

    harga_satuan = forms.IntegerField(label = 'Harga Satuan', widget = forms.NumberInput(attrs={
        'class':'form-control',
        'id':'harga_satuan',
    }), initial = 0, required=True)

    berat_satuan = forms.IntegerField(label = 'Berat Satuan', widget = forms.NumberInput(attrs={
        'class':'form-control',
        'id':'berat_satuan',
    }), initial = 0, required=True)
