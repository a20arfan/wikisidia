from django.shortcuts import render, reverse, redirect
from django.db import connection
from collections import namedtuple
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from datetime import date
from .forms import *
from django.db import IntegrityError

# Create your views here.

# general function
def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()] 

# read itemSumberDaya
def readItemSD(request):
    cursor = connection.cursor()
    role_admin_satgas = False
    role_supplier = False
    role_petugas_distribusi = False
    role_petugas_faskes = False

    try:
        if (request.session['role'] == 'admin_satgas'):
            role_admin_satgas = True
        if (request.session['role'] == 'supplier'):
            role_supplier = True
        if (request.session['role'] == 'petugas_distribusi'):
            role_petugas_distribusi = True
        if (request.session['role'] == 'petugas_faskes'):
            role_petugas_faskes = True
    except:
        return redirect('/')

    username = str(request.session['username'])
    cursor.execute("set search_path to wikisidia")
    query = "select its.kode as kode, username_supplier, its.nama as nama, harga_satuan, berat_satuan, ti.nama as nama_tipe_item "
    query += " from item_sumber_daya its"
    query += " left join tipe_item ti on its.kode_tipe_item = ti.kode"
    cursor.execute(query)
    hasil = namedtuplefetchall(cursor)
    cursor.close()
    argument = {
        'table' : hasil,
        'username' : username,
        'role_admin_satgas' : role_admin_satgas,
        'role_supplier' : role_supplier,
        'role_petugas_distribusi' : role_petugas_distribusi,
        'role_petugas_faskes' : role_petugas_faskes,
    }
    return render(request, "readItemSumberDaya.html", argument)

# delete item sumber daya
def deleteItemSumberDaya(request, kode):
    kode = str(kode)
    cursor = connection.cursor()
    role_supplier = False

    try:
        if (request.session['role'] == 'supplier'):
            role_supplier = True
    except:
        return redirect('/')

    username = str(request.session['username'])
    cursor.execute("set search_path to wikisidia")
    query = "delete from item_sumber_daya"
    query += (" where kode = '" + kode + "' ")
    query += (" and username_supplier = '"+ username +"'")
    cursor.execute(query)
    cursor.execute("select * from item_sumber_daya")
    hasil = namedtuplefetchall(cursor)
    cursor.close()
    message = "Data berhasil dihapus"
    argument = {
        'table' : hasil,
        'role_supplier' : role_supplier,
        'message' : message
    }
    return render(request, "readItemSumberDaya.html", argument)

# get input kode item sumber daya
def getInputKodeItemSD(request):
    input = request.POST
    kode = input['kode_item']
    return [kode]

# get input nama item sumber daya
def getInputNamaItemSD(request):
    input = request.POST
    nama = input['nama_item']
    return [nama]

# get input harga satuan item sumber daya
def getInputHargaSatuanSD(request):
    input = request.POST
    harga_satuan = input['harga_satuan']
    return [harga_satuan]

# get input berat satuan item sumber daya
def getInputBeratSatuanSD(request):
    input = request.POST
    berat_satuan = input['berat_satuan']
    return [berat_satuan]

# create new item sumber daya
def createItemSumberDaya(request):
    message =""
    cursor = connection.cursor()
    username = str(request.session['username'])
    nama_supplier = username
    cursor.execute("set search_path to wikisidia")
    cursor.execute("select kode from tipe_item")
    kode_tipe_item_view = namedtuplefetchall(cursor)

    if request.method =='POST':
        inputKode = getInputKodeItemSD(request)
        inputNama = getInputNamaItemSD(request)
        inputHarga = getInputHargaSatuanSD(request)
        inputBerat = getInputBeratSatuanSD(request)
        kode_tipe_item = request.POST['kode_tipe_item']
        try :
            # try:
            cursor.execute("insert into item_sumber_daya values ('"+ inputKode[0] +"','"+ nama_supplier +"','"+ inputNama[0] +"', '"+ inputHarga[0] +"', '"+ inputBerat[0] +"', '"+ kode_tipe_item +"')")
            cursor.close()
            message = "Item '"+ inputNama[0] +"' berhasil tersimpan."
            return redirect('/ItemSumberDaya')
        except IntegrityError:
            message = "Item dengan kode '"+ inputKode[0] +"' sudah tersedia."
        except :
            message = "Terdapat data yang belum diisi, silahkan lengkapi data terlebih dahulu."

    form = createItemSumberDayaForms(initial={
        'nama_supplier' : nama_supplier
    })
    form.fields['nama_supplier'].widget.attrs['disabled'] = 'disabled'
    args = {
        'kode_tipe_item' : kode_tipe_item_view,
        # 'supplier' : supplier_view,
        'nama_supplier' : nama_supplier,
        'form': form,
        'message' : message
    }
    return render(request, "createItemSumberDaya.html", args)

# update new item sumber daya
def updateItemSumberDaya(request, kode):
    message = ""
    kode = str(kode)
    cursor = connection.cursor()
    username = str(request.session['username'])
    nama_supplier = username
    cursor.execute("set search_path to wikisidia")
    cursor.execute("select kode from tipe_item")
    kode_tipe_item_view = namedtuplefetchall(cursor)

    if request.method =='POST':
        # inputKode = getInputKodeItemSD(request)
        # inputNama = getInputNamaItemSD(request)
        # inputHarga = getInputHargaSatuanSD(request)
        # inputBerat = getInputBeratSatuanSD(request)
        # kode_tipe_item = request.POST['kode_tipe_item']
        try :
            input = request.POST
            print(request.POST)
            cursor = connection.cursor()
            cursor.execute("set search_path to wikisidia")
            query = "update item_sumber_daya"
            query += " set kode = '"+ kode +"',"
            query += " username_supplier = '"+ nama_supplier +"',"
            query += " nama = '"+ input['nama_item'] +"',"
            query += " harga_satuan = '"+ input['harga_satuan'] +"',"
            query += " berat_satuan = '"+ input['berat_satuan'] +"',"
            query += " kode_tipe_item = '"+ input['kode_tipe_item'] +"'"
            query += (" where kode = '"+ kode +"'")
            cursor.execute(query)
            cursor.close()
            # message = "Item '"+ inputNama[0] +"' berhasil tersimpan."
            return redirect('/ItemSumberDaya')
        except IntegrityError:
            message = "Item dengan kode '"+ kode +"' sudah tersedia."
        except :
            message = "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu."

    query = "select * from item_sumber_daya where kode = '"+ kode +"'"
    cursor.execute(query)
    hasil = namedtuplefetchall(cursor)
    cursor.close()

    form = updateItemSumberDayaForms(initial={
        'kode_item' : kode,
        'nama_supplier' : nama_supplier,
        'nama_item' :  hasil[0].nama,
        'harga_satuan' : hasil[0].harga_satuan,
        'berat_satuan' : hasil[0].berat_satuan,
        # 'kode_tipe_item' : hasil[0].kode_tipe_item
    })

    form.fields['nama_supplier'].widget.attrs['disabled'] = 'disabled'

    args = {
        'kode' : kode,
        'kode_tipe_item' : kode_tipe_item_view,
        # 'supplier' : supplier_view,
        'nama_supplier' : nama_supplier,
        # 'hasil' : hasil,
        'form': form,
        'message' : message
    }
    return render(request, "updateItemSumberDaya.html", args)

# generate otomatis nama tipe faskes
def getTipeItem(request, kode_tipe_item):
    cursor = connection.cursor()
    cursor.execute("set search_path to wikisidia")
    cursor.execute("select nama from tipe_item where kode = '"+ kode_tipe_item +"'")
    hasil = namedtuplefetchall(cursor)
    cursor.close()
    argument = {
        'table' : hasil,
    }

    return JsonResponse(hasil, safe=False)
