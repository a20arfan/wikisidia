from django.contrib import admin
from django.urls import path
from .views import createItemSumberDaya, deleteItemSumberDaya, getTipeItem, readItemSD, updateItemSumberDaya

urlpatterns = [
    path('ItemSumberDaya/', readItemSD, name='readItemSD'),
    path('ItemSumberDaya/buatItemSumberDaya/', createItemSumberDaya, name='createItemSumberDaya'),
    path('ItemSumberDaya/updateItemSumberDaya/<str:kode>', updateItemSumberDaya, name='updateItemSumberDaya'),
    path('deleteItemSumberDaya/<str:kode>', deleteItemSumberDaya, name='deleteItemSumberDaya'),
    path('getTipeItem/<str:kode_tipe_item>', getTipeItem, name="getTipeItem"),
]