from django.shortcuts import render, redirect
from .forms import LoginForm, ChoiceRoleForm, AdminSatgasRoleForm, SupplierRoleForm, PetugasDistribusiRoleForm, PetugasFaskesRoleForm
from django.db import connection
from collections import namedtuple

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def index(request, validasi = None):
    message = ""
    try:
        username = request.session['username']
        return login(request)
    except KeyError:
        formulir = LoginForm()
        message = ""
        if(validasi==False):
            message = "Invalid username or password"
        argument = { 
            'formlogin' : formulir,
            'message' : message
        }
        return render(request, 'login.html', argument)
    
def login(request):
    try:
        username = request.session['username']
        password = request.session['password']
    except:
        username = request.POST['username']
        password = request.POST['password']

    username = str(username)
    password = str(password)
    cursor = connection.cursor()
    cursor.execute("set search_path to wikisidia")
    cursor.execute("select * from pengguna where username='"+username+"' and password='"+password+"'")
    hasil = namedtuplefetchall(cursor)
    role = cekRole(username)
    role_admin_satgas = False
    role_supplier = False
    role_petugas_distribusi = False
    role_petugas_faskes = False
    if (role == 'admin_satgas'):
        role_admin_satgas = True
    if (role == 'supplier'):
        role_supplier = True
    if (role == 'petugas_distribusi'):
        role_petugas_distribusi = True
    if (role == 'petugas_faskes'):
        role_petugas_faskes = True

    cursor.execute("set search_path to public")
    if (hasil == []):
        cursor.close()
        return index(request, False)
    else:
        request.session['username'] = hasil[0].username
        request.session['no_telepon'] = hasil[0].no_telepon
        request.session['password'] = hasil[0].password
        request.session['nama'] = hasil[0].nama
        request.session.set_expiry(1800)
        request.session['role'] = role
        argument = {
            'hasil' : hasil,
            'role_admin_satgas' : role_admin_satgas,
            'role_supplier' : role_supplier,
            'role_petugas_distribusi' : role_petugas_distribusi,
            'role_petugas_faskes' : role_petugas_faskes
        }
        cursor.close()
        return render(request, "home.html", argument)

def logout(request):
    request.session.flush()
    request.session.clear_expired()
    return index(request)

def cekRole(username):
    cursor = connection.cursor()
    cursor.execute("set search_path to wikisidia")
    cursor.execute("select * from pengguna where username='"+username+"'")
    pengguna = namedtuplefetchall(cursor)
    cursor.execute("select * from admin_satgas where username='"+username+"'")
    hasil = namedtuplefetchall(cursor)
    if (hasil != []):
        cursor.close()
        return "admin_satgas"
    else:
        cursor.execute("select * from petugas_distribusi where username='"+username+"'")
        hasil = namedtuplefetchall(cursor)
        if (hasil != []):
            cursor.close()
            return "petugas_distribusi"
        else:
            cursor.execute("select * from supplier where username='"+username+"'")
            hasil = namedtuplefetchall(cursor)
            if (hasil != []):
                cursor.close()
                return "supplier"
            else:
                cursor.execute("select * from petugas_faskes where username='"+username+"'")
                hasil = namedtuplefetchall(cursor)
                if (hasil != []):
                    cursor.close()
                    return "petugas_faskes"

def registerPengguna(request):
    formulir = ChoiceRoleForm()
    argument = { 
        'form' : formulir,
        'formrole' : True,
        'role_admin_satgas' : False,
        'role_supplier' : False,
        'role_petugas_distribusi' : False,
        'role_petugas_faskes' : False
    }
    return render(request, 'registerPengguna.html', argument)

def registerPenggunaRole(request, message="", role=None):
    try:
        Role = str(request.POST['Role'])
    except:
        Role = role
    role_admin_satgas = False
    role_supplier = False
    role_petugas_distribusi = False
    role_petugas_faskes = False
    if(Role == "admin_satgas"):
        formulir = AdminSatgasRoleForm()
        role_admin_satgas = True
    elif(Role == "supplier"):
        formulir = SupplierRoleForm()
        role_supplier = True
    elif(Role == "petugas_distribusi"):
        formulir = PetugasDistribusiRoleForm()
        role_petugas_distribusi = True
    else :
        formulir = PetugasFaskesRoleForm()
        role_petugas_faskes = True

    argument = { 
        'form' : formulir,
        'role' : Role,
        'role_admin_satgas' : role_admin_satgas,
        'role_supplier' : role_supplier,
        'role_petugas_distribusi' : role_petugas_distribusi,
        'role_petugas_faskes' : role_petugas_faskes,
        'message': message
    }
    return render(request, 'registerPengguna.html', argument)

def insert_admin_satgas(request):
    username = str(request.POST['username'])
    password = str(request.POST['password'])
    nama = str(request.POST['nama'])
    alamat_kel = str(request.POST['alamat_kel'])
    alamat_kec = str(request.POST['alamat_kec'])
    alamat_kabkot = str(request.POST['alamat_kabkot'])
    alamat_prov = str(request.POST['alamat_prov'])
    no_telepon = str(request.POST['no_telepon'])

    cursor = connection.cursor()
    cursor.execute("set search_path to wikisidia")
    cursor.execute("select count(*) as c from admin_satgas")
    hasil = namedtuplefetchall(cursor)
    # hasil2 = int(hasil[0].c)
    cursor.execute("select * from pengguna where username='"+username+"'")
    hasil = namedtuplefetchall(cursor)
    if (hasil == []):
        cursor.execute("insert into pengguna values ('"+username+"','"+password+"','"+nama+"','"+alamat_kel+"','"+alamat_kec+"','"+alamat_kabkot+"','"+alamat_prov+"','"+no_telepon+"')")
        while True:
            try:
                cursor.execute("insert into admin_satgas values ('"+username+"')")
                break
            except Exception as e:
                continue
    else:
        cursor.close()
        return registerPenggunaRole(request, "Username sudah terdaftar dengan peran Admin Satgas", 'admin_satgas')
                
    cursor.execute("select * from pengguna where username='"+username+"' and password='"+password+"'")
    hasil = namedtuplefetchall(cursor)

    if (hasil == []):
        cursor.close()
        return index(request, False)
    else:
        cursor.execute("set search_path to public")
        request.session['username'] = hasil[0].username
        request.session['password'] = hasil[0].password
        request.session['nama'] = hasil[0].nama
        request.session['alamat_kel'] = hasil[0].alamat_kel
        request.session['alamat_kec'] = hasil[0].alamat_kec
        request.session['alamat_prov'] = hasil[0].alamat_prov
        request.session['no_telepon'] = hasil[0].no_telepon
        request.session.set_expiry(1800)
        request.session['role'] = 'admin_satgas'
        argument = {
            'hasil' : hasil,
            'role_admin_satgas' : True,
            'role_supplier' : False,
            'role_petugas_distribusi' : False,
            'role_petugas_faskes' : False
        }
        cursor.close()
        return render(request, "home.html", argument)

def insert_petugas_distribusi(request):
    username = str(request.POST['username'])
    password = str(request.POST['password'])
    nama = str(request.POST['nama'])
    alamat_kel = str(request.POST['alamat_kel'])
    alamat_kec = str(request.POST['alamat_kec'])
    alamat_kabkot = str(request.POST['alamat_kabkot'])
    alamat_prov = str(request.POST['alamat_prov'])
    no_telepon = str(request.POST['no_telepon'])
    no_sim = str(request.POST['no_sim'])

    cursor = connection.cursor()
    cursor.execute("set search_path to wikisidia")

    cursor.execute("select * from pengguna where username='"+username+"'")
    hasil = namedtuplefetchall(cursor)
    if (hasil != []):
        cursor.close()
        return registerPenggunaRole(request, "Email sudah digunakan", 'petugas_distribusi')

    cursor.execute("insert into pengguna values ('"+username+"','"+password+"','"+nama+"','"+alamat_kel+"','"+alamat_kec+"','"+alamat_kabkot+"','"+alamat_prov+"','"+no_telepon+"')")
    cursor.execute("insert into petugas_distribusi values ('"+username+"','"+no_sim+"')")
    
    cursor.execute("select * from pengguna where username='"+username+"' and password='"+password+"'")
    hasil = namedtuplefetchall(cursor)

    if (hasil == []):
        cursor.close()
        return index(request, False)
    else:
        cursor.execute("set search_path to public")
        request.session['username'] = hasil[0].username
        request.session['no_telepon'] = hasil[0].no_telepon
        request.session['password'] = hasil[0].password
        request.session['nama'] = hasil[0].nama
        request.session.set_expiry(1800)
        request.session['role'] = 'petugas_distribusi'
        argument = {
            'hasil' : hasil,
            'role_admin_satgas' : False,
            'role_petugas_faskes' : False,
            'role_petugas_distribusi' : True,
            'role_supplier' : False
        }
        cursor.close()
        return render(request, "home.html", argument)

def insert_petugas_faskes(request):
    username = str(request.POST['username'])
    password = str(request.POST['password'])
    nama = str(request.POST['nama'])
    alamat_kel = str(request.POST['alamat_kel'])
    alamat_kec = str(request.POST['alamat_kec'])
    alamat_kabkot = str(request.POST['alamat_kabkot'])
    alamat_prov = str(request.POST['alamat_prov'])
    no_telepon = str(request.POST['no_telepon'])
    
    cursor = connection.cursor()
    cursor.execute("set search_path to wikisidia")

    cursor.execute("select * from pengguna where username='"+username+"'")
    hasil = namedtuplefetchall(cursor)
    if (hasil == []):
        cursor.execute("select username from petugas_faskes where username='"+username+"'")
        hasil = namedtuplefetchall(cursor)
        if (hasil == []):
            cursor.execute("insert into pengguna values ('"+username+"','"+password+"','"+nama+"','"+alamat_kel+"','"+alamat_kec+"','"+alamat_kabkot+"','"+alamat_prov+"','"+no_telepon+"')")
            cursor.execute("insert into petugas_faskes values ('"+username+"')")
           
        else:
            cursor.close()
            return registerPenggunaRole(request, "username petugas faskes sudah terdaftar", "petugas_faskes")
    else:
        cursor.close()
        return registerPenggunaRole(request, "Username sudah terdaftar dengan peran Petugas Faskes", "petugas_faskes")
                
    cursor.execute("select * from pengguna where username='"+username+"' and password='"+password+"'")
    hasil = namedtuplefetchall(cursor)

    if (hasil == []):
        cursor.close()
        return index(request, False)
    else:
        cursor.execute("set search_path to public")
        request.session['username'] = hasil[0].username
        request.session['no_telepon'] = hasil[0].no_telepon
        request.session['password'] = hasil[0].password
        request.session['nama'] = hasil[0].nama
        request.session.set_expiry(1800)
        request.session['role'] = 'petugas_faskes'
        argument = {
            'hasil' : hasil,
            'role_admin_satgas' : False,
            'role_supplier' : False,
            'role_petugas_distribusi' : False,
            'role_petugas_faskes' : True
        }
        
        cursor.close()
        return render(request, "home.html", argument)
    
def insert_supplier(request):
    nama_organisasi = str(request.POST['nama_organisasi'])
    username = str(request.POST['username'])
    password = str(request.POST['password'])
    nama = str(request.POST['nama'])
    alamat_kel = str(request.POST['alamat_kel'])
    alamat_kec = str(request.POST['alamat_kec'])
    alamat_kabkot = str(request.POST['alamat_kabkot'])
    alamat_prov = str(request.POST['alamat_prov'])
    no_telepon = str(request.POST['no_telepon'])
    
    cursor = connection.cursor()
    cursor.execute("set search_path to wikisidia")

    cursor.execute("select * from pengguna where username='"+username+"'")
    hasil = namedtuplefetchall(cursor)
    if (hasil == []):
        cursor.execute("select username from petugas_faskes where username='"+username+"'")
        hasil = namedtuplefetchall(cursor)
        if (hasil == []):
            cursor.execute("insert into pengguna values ('"+username+"','"+password+"','"+nama+"','"+alamat_kel+"','"+alamat_kec+"','"+alamat_kabkot+"','"+alamat_prov+"','"+no_telepon+"')")
            cursor.execute("insert into supplier values ('"+username+"','"+nama_organisasi+"')")
           
        else:
            cursor.close()
            return registerPenggunaRole(request, "username supplier sudah terdaftar", "supplier")
    else:
        cursor.close()
        return registerPenggunaRole(request, "Username sudah terdaftar dengan peran Supplier", "supplier")
                
    cursor.execute("select * from pengguna where username='"+username+"' and password='"+password+"'")
    hasil = namedtuplefetchall(cursor)

    if (hasil == []):
        cursor.close()
        return index(request, False)
    else:
        cursor.execute("set search_path to public")
        request.session['username'] = hasil[0].username
        request.session['no_telepon'] = hasil[0].no_telepon
        request.session['password'] = hasil[0].password
        request.session['nama_lengkap'] = hasil[0].nama_lengkap
        request.session.set_expiry(1800)
        request.session['role'] = 'supplier'
        argument = {
            'hasil' : hasil,
            'role_admin_satgas' : False,
            'role_supplier' : True,
            'role_petugas_distribusi' : False,
            'role_petugas_faskes' : False
        }
        
        cursor.close()
        return render(request, "home.html", argument)

def profile(request):
    try:
        username = request.session['username']
    except Exception as e:
        return redirect('/')
    cursor = connection.cursor()
    cursor.execute("set search_path to wikisidia")
    cursor.execute("select * from pengguna where username='"+username+"'")
    pengguna = namedtuplefetchall(cursor)

    role = cekRole(username)
    role_admin_satgas = False
    role_supplier = False
    role_petugas_distribusi = False
    role_petugas_faskes = False
    if (role == 'admin_satgas'):
        role_admin_satgas = True
    if (role == 'supplier'):
        role_supplier = True
    if (role == 'petugas_distribusi'):
        role_petugas_distribusi = True
    if (role == 'petugas_faskes'):
        role_petugas_faskes = True
        
    if (role_admin_satgas):
        cursor.execute("select * from admin_satgas where username='"+username+"'")

        argument = { 
            'hasil' : pengguna,
            'role' : role,
            'roleadmin_satgas' : role_admin_satgas,
            'rolesupplier' : role_supplier,
            'rolepetugas_distribusi' : role_petugas_distribusi,
            'rolepetugas_faskes' : role_petugas_faskes,
        }

    else:
        argument = {
            'hasil' : pengguna,
            'role' : role,
            'roleadmin_satgas' : role_admin_satgas,
            'rolesupplier' : role_supplier,
            'rolepetugas_distribusi' : role_petugas_distribusi,
            'rolepetugas_faskes' : role_petugas_faskes,
        }

    cursor.close()
    return render(request, "profile.html", argument)
