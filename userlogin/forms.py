from django import forms

PROVINSI_CHOICE=[
    ('Aceh', 'Aceh'),
	('Sumatra Utara', 'Sumatra Utara'),
	('Sumatra Barat', 'Sumatra Barat'),
	('Riau', 'Riau'),
	('Kepulauan Riau', 'Kepulauan Riau'),
	('Jambi', 'Jambi'),
	('Bengkulu', 'Bengkulu'),
	('Sumatra Selatan', 'Sumatra Selatan'),
	('Kepulauan Bangka Belitung', 'Kepulauan Bangka Belitung'),
    ('Lampung', 'Lampung'),
    ('Banten', 'Banten'),
    ('Jawa Barat', 'Jawa Barat'),
    ('DKI Jakarta', 'DKI Jakarta'),
    ('Jawa Tengah', 'Jawa Tengah'),
    ('DIY Yogyakarta', 'DIY Yogyakarta'),
    ('Jawa Timur', 'Jawa Timur'),
    ('Bali', 'Bali'),
    ('Nusa Tenggara Barat', 'Nusa Tenggara Barat'),
    ('Nusa Tenggara Timur', 'Nusa Tenggara Timur'),
    ('Kalimantan Barat', 'Kalimantan Barat'),
    ('Kalimantan Selatan', 'Kalimantan Selatan'),
    ('Kalimantan Tengah', 'Kalimantan Tengah'),
    ('Kalimantan Timur', 'Kalimantan Timur'),
    ('Kalimantan Utara', 'Kalimantan Utara'),
    ('Gorontalo', 'Gorontalo'),
    ('Sulawesi Barat', 'Sulawesi Barat'),
    ('Sulawesi Selatan', 'Sulawesi Selatan'),
    ('Sulawesi Tengah', 'Sulawesi Tengah'),
    ('Sulawesi Tenggara', 'Sulawesi Tenggara'),
    ('Sulawesi Utara', 'Sulawesi Utara'),
    ('Maluku', 'Maluku'),
    ('Maluku Utara', 'Maluku Utara'),
    ('Papua Barat', 'Papua Barat'),
    ('Papua', 'Papua'),
]

class LoginForm(forms.Form):
    username = forms.CharField(label = 'Username', max_length=50, widget = forms.TextInput(attrs = {
        'class' : 'form-control',
        'type' : 'text',
        'required' : True,
    }))
    password = forms.CharField(label = 'Password', max_length=128, widget = forms.PasswordInput(attrs = {
        'class' : 'form-control',
        'type' : 'password',
        'required' : True,
    }))

class ChoiceRoleForm(forms.Form):
    CHOICES=[
        ('Admin','Admin'),
        ('Supplier','Supplier'),
        ('PetugasDistribusi','PetugasDistribusi'),
        ('PetugasFaskes','PetugasFaskes')]
    Role = forms.ChoiceField(choices=CHOICES, widget=forms.RadioSelect)

class AdminSatgasRoleForm(forms.Form):
    username = forms.CharField(label = 'Username', max_length=50, widget = forms.TextInput(attrs = {
        'class' : 'form-control',
        'type' : 'text',
        'required' : True,
    }))
    password = forms.CharField(label = 'Password', max_length=128, widget = forms.PasswordInput(attrs = {
        'class' : 'form-control',
        'type' : 'password',
        'required' : True,
    }))
    nama = forms.CharField(label = 'Nama Lengkap', max_length=50, widget = forms.TextInput(attrs = {
        'class' : 'form-control',
        'type' : 'text',
        'required' : True,
    }))
    alamat_kel = forms.CharField(label = 'Kelurahan', max_length=30, widget = forms.TextInput(attrs = {
        'class' : 'form-control',
        'type' : 'text',
        'required' : True,
    }))
    alamat_kec = forms.CharField(label='Kecamatan', max_length=20, widget = forms.TextInput(attrs = {
        'class' : 'form-control',
        'type' : 'text',
        'required' : True,
    }))
    alamat_kabkot = forms.CharField(label='Kabupaten/Kota', max_length=20, widget = forms.TextInput(attrs = {
        'class' : 'form-control',
        'type' : 'text',
        'required' : True,
    }))
    alamat_prov = forms.CharField(label='Provinsi', widget=forms.Select(choices=PROVINSI_CHOICE, attrs = {
        'class' : 'form-control',
        'type' : 'text',
        'required' : True,
    }))
    no_telepon = forms.CharField(label='No. Telepon', max_length=15, widget = forms.TextInput(attrs = {
        'class' : 'form-control',
        'type' : 'text',
        'required' : True,
    }))

class SupplierRoleForm(forms.Form):
    nama_organisasi = forms.CharField(label = 'Nama Organisasi', max_length=50, widget = forms.TextInput(attrs = {
        'class' : 'form-control',
        'type' : 'text',
        'required' : True,
    }))
    username = forms.CharField(label='Username', max_length=50, widget = forms.TextInput(attrs = {
        'class' : 'form-control',
        'type' : 'text',
        'required' : True,
    }))
    password = forms.CharField(label = 'Password', max_length=128, widget = forms.PasswordInput(attrs = {
        'class' : 'form-control',
        'type' : 'password',
        'required' : True,
    }))
    nama = forms.CharField(label = 'Nama Lengkap', max_length=50, widget = forms.TextInput(attrs = {
        'class' : 'form-control',
        'type' : 'text',
        'required' : True,
    }))
    alamat_kel = forms.CharField(label = 'Kelurahan', max_length=30, widget = forms.TextInput(attrs = {
        'class' : 'form-control',
        'type' : 'text',
        'required' : True,
    }))
    alamat_kec = forms.CharField(label='Kecamatan', max_length=20, widget = forms.TextInput(attrs = {
        'class' : 'form-control',
        'type' : 'text',
        'required' : True,
    }))
    alamat_kabkot = forms.CharField(label='Kabupaten/Kota', max_length=20, widget = forms.TextInput(attrs = {
        'class' : 'form-control',
        'type' : 'text',
        'required' : True,
    }))
    alamat_prov = forms.CharField(label='Provinsi', widget=forms.Select(choices=PROVINSI_CHOICE, attrs = {
        'class' : 'form-control',
        'type' : 'text',
        'required' : True,
    }))
    no_telepon = forms.CharField(label = 'No Telepon', max_length=20, widget = forms.TextInput(attrs = {
        'class' : 'form-control',
        'type' : 'text',
        'required' : True,
    }))

class PetugasDistribusiRoleForm(forms.Form):
    username = forms.CharField(label = 'Username', max_length=50, widget = forms.TextInput(attrs = {
        'class' : 'form-control',
        'type' : 'text',
        'required' : True,
    }))
    password = forms.CharField(label = 'Password', max_length=128, widget = forms.PasswordInput(attrs = {
        'class' : 'form-control',
        'type' : 'password',
        'required' : True,
    }))
    nama = forms.CharField(label = 'Nama Lengkap', max_length=50, widget = forms.TextInput(attrs = {
        'class' : 'form-control',
        'type' : 'text',
        'required' : True,
    }))
    alamat_kel = forms.CharField(label = 'Kelurahan', max_length=30, widget = forms.TextInput(attrs = {
        'class' : 'form-control',
        'type' : 'text',
        'required' : True,
    }))
    alamat_kec = forms.CharField(label='Kecamatan', max_length=20, widget = forms.TextInput(attrs = {
        'class' : 'form-control',
        'type' : 'text',
        'required' : True,
    }))
    alamat_kabkot = forms.CharField(label='Kabupaten/Kota', max_length=20, widget = forms.TextInput(attrs = {
        'class' : 'form-control',
        'type' : 'text',
        'required' : True,
    }))
    alamat_prov = forms.CharField(label='Provinsi', widget=forms.Select(choices=PROVINSI_CHOICE, attrs = {
        'class' : 'form-control',
        'type' : 'text',
        'required' : True,
    }))
    no_telepon = forms.CharField(label = 'No Telepon', max_length=20, widget = forms.TextInput(attrs = {
        'class' : 'form-control',
        'type' : 'text',
        'required' : True,
    }))
    no_sim = forms.CharField(label = 'No. SIM', max_length=50, widget = forms.TextInput(attrs = {
        'class' : 'form-control',
        'type' : 'text',
        'required' : True,
    }))

class PetugasFaskesRoleForm(forms.Form):
    username = forms.CharField(label = 'Username', max_length=50, widget = forms.TextInput(attrs = {
        'class' : 'form-control',
        'type' : 'text',
        'required' : True,
    }))
    password = forms.CharField(label = 'Password', max_length=128, widget = forms.PasswordInput(attrs = {
        'class' : 'form-control',
        'type' : 'password',
        'required' : True,
    }))
    nama = forms.CharField(label = 'Nama Lengkap', max_length=50, widget = forms.TextInput(attrs = {
        'class' : 'form-control',
        'type' : 'text',
        'required' : True,
    }))
    alamat_kel = forms.CharField(label = 'Kelurahan', max_length=30, widget = forms.TextInput(attrs = {
        'class' : 'form-control',
        'type' : 'text',
        'required' : True,
    }))
    alamat_kec = forms.CharField(label='Kecamatan', max_length=20, widget = forms.TextInput(attrs = {
        'class' : 'form-control',
        'type' : 'text',
        'required' : True,
    }))
    alamat_kabkot = forms.CharField(label='Kabupaten/Kota', max_length=20, widget = forms.TextInput(attrs = {
        'class' : 'form-control',
        'type' : 'text',
        'required' : True,
    }))
    alamat_prov = forms.CharField(label='Provinsi', widget=forms.Select(choices=PROVINSI_CHOICE, attrs = {
        'class' : 'form-control',
        'type' : 'text',
        'required' : True,
    }))
    no_telepon = forms.CharField(label = 'No Telepon', max_length=20, widget = forms.TextInput(attrs = {
        'class' : 'form-control',
        'type' : 'text',
        'required' : True,
    }))
