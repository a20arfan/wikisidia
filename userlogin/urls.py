from django.contrib import admin
from django.urls import path
from .views import index, login, profile, registerPengguna, registerPenggunaRole, insert_admin_satgas, insert_supplier, insert_petugas_distribusi, logout, insert_petugas_faskes

urlpatterns = [
    path('', index, name='login'),
    path('Home/', login),
    path('Profile/', profile),
    path('Logout/', logout,  name='logout'),
    path('Register/', registerPengguna),
    path('RegisterRole/', registerPenggunaRole),
    path('insertadmin_satgas/', insert_admin_satgas),
    path('insertsupplier/', insert_supplier),
    path('insertpetugas_distribusi/', insert_petugas_distribusi),
    path('insertpetugas_faskes/', insert_petugas_faskes),

]